﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace Claiming_The_Throne_Simulator
{
    public class Sound
    {
        public static string BtnSelPath => BaseDirectory + "450614__breviceps__8-bit-collect-sound.wav";
        public static string SceneSwitchPath => BaseDirectory + "253172__suntemple__retro-bonus-pickup-sfx.wav";
        public static string DeniedSelPath => BaseDirectory + "196979__peepholecircus__sci-fi-button-beep.wav";
        public static string KeyDownPath => BaseDirectory + "421583__uberbosser__qkey.wav";
        public static string NormalSelPath => BaseDirectory +"196979__peepholecircus__sci-fi-button-beep.wav";

        public static string BaseDirectory => AppDomain.CurrentDomain.BaseDirectory + "Sounds\\";

        static SoundPlayer m_soundPlayer;

        public static void PlayOneShot(string soundPath, bool sync = false)
        {
            if (m_soundPlayer == null)
                m_soundPlayer = new SoundPlayer();
            m_soundPlayer.SoundLocation = soundPath;
            if (!sync)
                m_soundPlayer.Play();
            else
                m_soundPlayer.PlaySync();
        }

        public static void Stop()
        {
            if (m_soundPlayer != null)
            {
                m_soundPlayer.Stop();
            }
        }
    }
}
