﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Claiming_The_Throne_Simulator
{
    public class CTTBoard
    {
        Dictionary<Vector2Int, Piece> m_board;

        public Piece this[Vector2Int position]
        {
            get
            {
                // If this position is an available location, return the slot.
                // Else, return null.
                if (m_board.ContainsKey(position))
                    return m_board[position];
                else
                    return new Piece() { Type = 0xff, Faction = 0xff };
            }
            set
            {
                var piece = this[position];
                if (!piece.IsInvalid)
                {
                    m_board[position] = value;
                }
                // If out of bound, nothing happens.
            }
        }

        public Piece this[int x, int y]
        {
            get
            {
                var v = new Vector2Int(x, y);
                return this[v];
            }
            set
            {
                var v = new Vector2Int(x, y);
                this[v] = value;
            }
        }

        public CTTBoard()
        {
            m_board = new Dictionary<Vector2Int, Piece>();
        }

        public void Reset()
        {
            // Creates a board with inital setting.
            SetupEmpty();

            // y = x faction (Left):
            for (int x = 3; x <= 5; x++)
                for (int y = -x + 8; y <= 5; y++)
                {
                    this[x, y] = new Piece(CTTType.Pawn, CTTFaction2.One);   // Supporter
                    this[-x, -y] = new Piece(CTTType.Pawn, CTTFaction2.One);   // Fief
                }
            // (-5, -5) is the prince.
            this[-5, -5] = new Piece(CTTType.Prince, CTTFaction2.One);
            this[-5, -3] = new Piece(CTTType.Knight, CTTFaction2.One);
            this[-4, -4] = new Piece(CTTType.Rook, CTTFaction2.One);
            this[-3, -5] = new Piece(CTTType.Knight, CTTFaction2.One);

            // y = -x faction (Right):
            for (int x = -10; x <= -8; x++)
                for (int y = -x - 5; y <= 5; y++)
                {
                    this[x, y] = new Piece(CTTType.Pawn, CTTFaction2.Two);   // Supporter
                    this[-x, -y] = new Piece(CTTType.Pawn, CTTFaction2.Two);   // Fief
                }
            // (10, -5) is the prince.
            this[10, -5] = new Piece(CTTType.Prince, CTTFaction2.Two);
            this[8, -3] = new Piece(CTTType.Knight, CTTFaction2.Two);
            this[8, -4] = new Piece(CTTType.Rook, CTTFaction2.Two);
            this[8, -5] = new Piece(CTTType.Knight, CTTFaction2.Two);
        }

        public void SetupEmpty()
        {
            m_board.Clear();

            // Create a hexagon of radius 8.
            int b = 8 - 1;
            for (int x = -b; x <= b; x++)
            {
                for (int y = Mathf.Max(-x - b, -b); y <= Math.Min(-x + b, b); y++)
                {
                    m_board.Add(new Vector2Int(x, y), Piece.Empty);
                }
            }

            // Add capital triangle:
            for (int x = -6; x <= -2; x++)
                for (int y = 8; y <= -x + 6; y++)
                {
                    m_board.Add(new Vector2Int(x, y), Piece.Empty);
                }

            // Add the y=-x faction (Right):
            for (int x = -10; x <= -8; x++)
                for (int y = -x - 5; y <= 5; y++)
                {
                    // The supporter's area for right
                    m_board.Add(new Vector2Int(x, y), Piece.Empty);

                    // The fief for right
                    m_board.Add(new Vector2Int(-x, -y), Piece.Empty);
                }

            // Add the y = x faction:
            for (int x = 3; x <= 5; x++)
                for (int y = -x + 8; y <= 5; y++)
                {
                    // The supporter's area for left
                    m_board.Add(new Vector2Int(x, y), Piece.Empty);

                    // The fief for left
                    m_board.Add(new Vector2Int(-x, -y), Piece.Empty);
                }
        }

        public CTTBoard Copy
        {
            get
            {
                CTTBoard copy = new CTTBoard();
                foreach (var pos in this.m_board.Keys)
                {
                    copy.m_board.Add(pos, this[pos]);
                }
                return copy;
            }
        }
    }

    public class Player
    {
        public bool EndTurn;
        public byte Faction { get => m_faction; set => m_faction = value; }

        byte m_faction = CTTFaction2.None;

        CTTBoard m_realBoard;
        CTTBoard m_mockBoard = null;
        bool m_mockBoardIsDirty = true;

        Piece m_selectedPiece = Piece.Null;
        Vector2Int m_selectedPos;
        public bool Selected => !m_selectedPiece.IsInvalid && !m_selectedPiece.IsEmpty;
        public Vector2Int SelectedPosition => m_selectedPos;
        public Piece SelectedPiece => m_selectedPiece;

        CTTMove m_currentMove;
        public bool Moved => m_currentMove.Count > 0;

        Dictionary<Vector2Int, CTTAvailableMove> m_currAvailableMoves = new Dictionary<Vector2Int, CTTAvailableMove>();

        public CTTBoard MockBoard => m_mockBoard;
        public bool MockBoardDirty => m_mockBoardIsDirty;

        public Player(CTTBoard board, byte faction)
        {
            m_realBoard = board;
            m_faction = faction;
        }

        /// <summary>
        /// If the mock board is dirty, copy. Otherwise, use the mock board.
        /// </summary>
        public void ResetMockBoard()
        {
            if (m_mockBoardIsDirty)
            {
                m_mockBoard = m_realBoard.Copy;
                m_mockBoardIsDirty = false;
            }
        }

        public void SelectNothing()
        {
            m_selectedPiece = Piece.Null;
            m_selectedPos = new Vector2Int();
        }

        public bool Select(Vector2Int position)
        {
            var p = m_realBoard[position];
            if (p.IsInvalid || p.IsEmpty) return false;

            // Can't select pieces of other factions.
            if (p.Faction != m_faction) return false;

            // Can select.
            m_selectedPiece = p;
            m_selectedPos = position;
            // Copy to the mock Board.
            ResetMockBoard();

            // Start the record of move.
            m_currentMove = new CTTMove()
            {
                Faction = this.m_faction,
                PieceType = m_selectedPiece.Type,
            };

            return true;
        }

        public Dictionary<Vector2Int, CTTAvailableMove> CalculateAvailable()
        {
            // Assume the selectedPawn is selected.
            Debug.Assert(Selected);

            m_currAvailableMoves = GameManager.CalculateAvailable(m_selectedPos, m_mockBoard, m_currentMove);
            return m_currAvailableMoves;
        }

        //Stack<CTTAvailableMove> m_mockedMoves = new Stack<CTTAvailableMove>();

        /// <summary>
        /// Go to the position among the available positions from CalculateAvailable()
        /// </summary>
        public void MockMove(Vector2Int position)
        {
            // Assume the selectedPawn is selected.
            Debug.Assert(Selected);
            // Assume the position is in the m_currAvailableMoves
            Debug.Assert(m_currAvailableMoves.ContainsKey(position));

            // Go to this position on the mock board.
            // Get that available move.
            var move = m_currAvailableMoves[position];
            // Go to the end position on the mock board.
            m_mockBoard[m_selectedPos] = Piece.Empty;
            m_mockBoard[m_selectedPos = move.EndPosition] = m_selectedPiece;
            // Remove the pieces on the mock board.
            foreach (var p in move.PiecesRemoved.Keys)
            {
                m_mockBoard[p] = Piece.Empty;
            }
            // Label as dirty.
            m_mockBoardIsDirty = true;

            // Add to the move record.
            m_currentMove.AddMove(move);
        }

        public void UndoMockMove()
        {
            // Assume the selectedPawn is selected.
            Debug.Assert(Selected);

            // Can't undo when no mocked moves.
            if (m_currentMove.Count == 0) return;

            var move = m_currentMove.LastMove;
            if (m_currentMove.UndoMove())
            {
                // Put the selected piece back.
                Debug.Assert(move.EndPosition == m_selectedPos);
                m_mockBoard[m_selectedPos] = Piece.Empty;
                m_mockBoard[m_selectedPos = move.StartPosition] = m_selectedPiece;

                // Put the captured pieces back.
                foreach (var p in move.PiecesRemoved)
                {
                    m_mockBoard[p.Key] = p.Value;
                }
                // Remain the same dirtyness status. 
            }
            // Won't succeed if there's no move.
        }

        /// <summary>
        /// Top is the first move. bottom is the last move.
        /// </summary>
        public Stack<CTTAvailableMove> MoveStack
        {
            get
            {
                var s = new Stack<CTTAvailableMove>();
                for (int i = m_currentMove.moves.Count - 1; i >= 0; i--)
                    s.Push(m_currentMove.moves[i]);
                return s;
            }
        }

        //public CTTMove ApplyMove()
        //{
        //    // Copy the mock board to the real board.
        //    m_realBoard = m_mockBoard.Copy;

        //    // No longer dirty.
        //    m_mockBoardIsDirty = false;

        //    // Record the move.
        //    // Clear the mockedMoves stack.
        //    var roundMove = new CTTMove()
        //    {
        //        Faction = this.m_faction,
        //        PieceType = m_selectedPiece.Type,
        //    };
        //    CTTAvailableMove move;
        //    while (m_mockedMoves.Count > 0)
        //    {
        //        move = m_mockedMoves.Pop();
        //        roundMove.EndPositionSequence.Push(move.EndPosition);
        //        if (m_mockedMoves.Count == 0)
        //        {
        //            // Last one.
        //            roundMove.StartPosition = move.StartPosition;
        //        }
        //    }
        //    return roundMove;
        //}
    }

    public struct Piece
    {
        public byte Type;    // Default: Empty
        public byte Faction;        // Default: No faction

        public static Piece Empty { get => new Piece(CTTType.Empty, CTTFaction2.None); }
        public static Piece Null { get => new Piece(CTTType.Null, CTTFaction2.None); }

        public Piece(byte type, byte faction)
        {
            this.Type = type;
            this.Faction = faction;
        }

        /// <summary>
        /// If it is Piece.Null
        /// </summary>
        public bool IsInvalid => (Type ^ 0xff) == 0;

        /// <summary>
        /// If it is Piece.Empty
        /// </summary>
        public bool IsEmpty => (Type ^ 0x00) == 0;
    }

    static class CTTType
    {
        public static byte Empty = 0x00;
        public static byte Null = 0xff;
        public static byte Pawn = 0x01;
        public static byte Knight = 0x02;
        public static byte Rook = 0x04;
        public static byte Prince = 0x08;

        public static string ToName(byte type)
        {
            switch (type)
            {
                case 0x00:
                    return "Empty";
                case 0xff:
                    return "Invalid";
                case 0x01:
                    return "Pawn";
                case 0x02:
                    return "Knight";
                case 0x04:
                    return "Rook";
                case 0x08:
                    return "Prince";
                default:
                    return string.Empty;
            }
        }
    }

    static class CTTFaction2
    {
        public const byte Player = 0x00;
        public const byte One = 0x01;
        public const byte Two = 0x02;
        public const byte None = 0xff;
    }
}