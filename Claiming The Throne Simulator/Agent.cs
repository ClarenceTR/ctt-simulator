﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Claiming_The_Throne_Simulator
{
    public enum CTTDirection
    {
        Right,
        Upright,
        Upleft,
        Left,
        Downleft,
        Downright,
        Invalid,
    }

    public abstract class Slot
    {
        protected BoardManager m_board;

        /// <summary>
        /// Create a slot and set up a board. Won't replace. Position is initialized at (0, 0).
        /// </summary>
        /// <param name="board"></param>
        public Slot(BoardManager board)
        {
            m_board = board;
        }
    }

    public class Empty : Slot
    {
        public Empty(BoardManager board) : base(board) { }
    }

    public class Agent : Slot
    {
        protected Vector2? m_position;

        /// <summary>
        /// Create an agent and set up a board. Won't place it onto this board. 
        /// </summary>
        /// <param name="board">The board to place on.</param>
        public Agent(BoardManager board) : base(board)
        {
            m_position = null;
        }

        /// <summary>
        /// Create an agent, set up and place onto a board. Will replace existing piece at the spot.
        /// If the position is not on a board, or the board parameter is null, m_position is still null.
        /// </summary>
        /// <param name="board"></param>
        /// <param name="position"></param>
        public Agent(BoardManager board, Vector2 position) : base(board)
        {
            m_position = null;
            // m_position will be set if position is on the board, or the board parameter is not null.
            Replace(position);
        }

        public virtual Vector2? Position
        {
            get => m_position; set => m_position = value;

        }

        public void Remove()
        {
            // Do nothing if I'm already off.
            if (!m_position.HasValue) return;

            // Set this position on board empty. Clear() will set me off.
            if (m_board != null)
                m_board.Clear(m_position.Value);
            else
                m_position = null;
        }

        /// <summary>
        /// A shortcut on Agent to place onto its board.
        /// </summary>
        /// <param name="position">The position to put at</param>
        public bool TryPlace(Vector2 position)
        {
            // Do nothing if I don't have a board.
            if (m_board == null) return false;

            return m_board.TryPlace(this, position);
        }

        // If position is out of the bounds, then the m_position is still null.
        public void Replace(Vector2 position)
        {
            // Replace if there's any stuff there.
            if (m_board != null)
            {
                m_board.Clear(position);
                m_board.ForcePlace(this, position);
            }
            // Else, no board so do nothing.
        }

        // Movements:
        /// <summary>
        /// If can move, it moves to that position and updates the board.
        /// Else, it doesn't move. Should be considered the move is not done.
        /// If the agent doesn't have a board, or is not on a board, return false as well.
        /// </summary>
        /// <param name="direction">The Direction the piece moves.</param>
        /// <returns>If the move is successful.</returns>
        public virtual bool TryMove(CTTDirection direction)
        {
            if (m_board == null || m_position == null) return false;

            // Move one step to that direction.
            // If can't move, do nothing.
            var expectedPosition = BoardManager.Translate(m_position.Value, direction, 1);

            Vector2 originalPos = m_position.Value;
            bool canIMove = m_board.TryPlace(this, expectedPosition); // If blocked, return false.
            return canIMove;
        }

        public virtual bool TryJump(CTTDirection direction)
        {
            var posFront = BoardManager.Translate(m_position.Value, direction, 1);
            var expectedPosition = BoardManager.Translate(m_position.Value, direction, 2);

            // If posFront not occupied, fail.
            if (m_board.IsPositionEmpty(posFront)) return false;
            // If expectedPosition occupied, fail.
            // Else, jump.
            bool canIJump = m_board.TryPlace(this, expectedPosition); // If blocked, return false.
            return canIJump;
        }
    }
}
