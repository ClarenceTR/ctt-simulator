﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Claiming_The_Throne_Simulator
{
    public class BoardManager
    {
        Dictionary<Vector2, Slot> m_availableLocs;
        public IEnumerable<Vector2> Board { get => m_availableLocs.Keys; }

        // Districts.
        List<Vector2> m_distLeftFief;   // y=x
        List<Vector2> m_distLeftSupport;
        List<Vector2> m_distRightFief;  // y=-x
        List<Vector2> m_distRightSupport;
        List<Vector2> m_distCapital;
        List<Vector2> m_distPalace; // Capital district include palace.

        public List<Vector2> DistLeftFief { get => m_distLeftFief; }
        public List<Vector2> DistLeftSupport { get => m_distLeftSupport; }
        public List<Vector2> DistRightFief { get => m_distRightFief; }
        public List<Vector2> DistRightSupport { get => m_distRightSupport; }
        public List<Vector2> DistCapital { get => m_distCapital; }
        public List<Vector2> DistPalace { get => m_distPalace; }

        public BoardManager()
        {
            m_availableLocs = new Dictionary<Vector2, Slot>();

            m_distLeftFief = new List<Vector2>();
            m_distLeftSupport = new List<Vector2>();
            m_distRightFief = new List<Vector2>();
            m_distRightSupport = new List<Vector2>();
            m_distCapital = new List<Vector2>();
            m_distPalace = new List<Vector2>();

            // Claiming The Throne has a hexagon with radius of 8.
            var hexagonLocs = CreateHexagon(8);
            foreach (var loc in hexagonLocs)
                m_availableLocs.Add(loc, new Empty(this));

            // Add capital triangle:
            for (int x = -6; x <= -2; x++)
                for (int y = 8; y <= -x + 6; y++)
                {
                    m_availableLocs.Add(new Vector2(x, y), new Empty(this));
                    m_distCapital.Add(new Vector2(x, y));
                }

            // Palace.
            m_distPalace.Add(new Vector2(-6, 12));

            // Add the y=-x faction (Right):
            for (int x = -10; x <= -8; x++)
                for (int y = -x - 5; y <= 5; y++)
                {
                    // The supporter's area for right
                    m_availableLocs.Add(new Vector2(x, y), new Empty(this));
                    m_distRightSupport.Add(new Vector2(x, y));

                    // The fief for right
                    m_availableLocs.Add(new Vector2(-x, -y), new Empty(this));
                    m_distRightFief.Add(new Vector2(-x, -y));
                }

            // Add the y = x faction:
            for (int x = 3; x <= 5; x++)
                for (int y = -x + 8; y <= 5; y++)
                {
                    // The supporter's area for left
                    m_availableLocs.Add(new Vector2(x, y), new Empty(this));
                    m_distLeftSupport.Add(new Vector2(x, y));

                    // The fief for left
                    m_availableLocs.Add(new Vector2(-x, -y), new Empty(this));
                    m_distLeftFief.Add(new Vector2(-x, -y));
                }
        }

        public List<Vector2> CreateHexagon(int radius)
        {
            if (radius < 1) return null;

            List<Vector2> res = new List<Vector2>();
            /*  <- YX axis      -> Y axis
             *      (-1, 1) (0, 1)
             *   (-1,0)  (0,0)  (1,0)   -> X axis
             *      (0,-1)  (1, -1)
             */
            int b = radius - 1;
            for (int x = -b; x <= b; x++)
            {
                for (int y = Mathf.Max(-x - b, -b); y <= Math.Min(-x + b, b); y++)
                {
                    res.Add(new Vector2(x, y));
                }
            }

            return res;
        }

        public static Vector2 Translate(Vector2 position, CTTDirection direction, int magnitude)
        {
            var res = position;
            magnitude = Math.Abs(magnitude);
            switch (direction)
            {
                // Right/Left: y=0;
                // Upright/Downleft: x=0;
                // Upleft/Downright: y=-x;

                case CTTDirection.Right:
                    res.x += magnitude;
                    break;
                case CTTDirection.Upright:
                    res.y += magnitude;
                    break;
                case CTTDirection.Upleft:
                    res.x -= magnitude;
                    res.y += magnitude;
                    break;
                case CTTDirection.Left:
                    res.x -= magnitude;
                    break;
                case CTTDirection.Downleft:
                    res.y -= magnitude;
                    break;
                case CTTDirection.Downright:
                    res.x += magnitude;
                    res.y -= magnitude;
                    break;
                default:
                    break;
            }
            return res;

        }

        public Slot this[Vector2 position]
        {
            get
            {
                // If this position is an available location, return the slot.
                // Else, return null.
                if (m_availableLocs.ContainsKey(position))
                    return m_availableLocs[position];
                else
                    return null;
            }
            set
            {
                if (m_availableLocs.ContainsKey(position))
                {
                    m_availableLocs[position] = value;
                }
                // If out of bound, nothing happens.
            }
        }

        public bool IsPositionEmpty(Vector2 position)
        {
            var pos = this[position];
            if (pos != null && pos is Empty)
                return true;
            return false;
        }

        /// <summary>
        /// Try to place "agent" at the "position on the board.
        /// If can, place and return true. Else, do nothing and return false.
        /// </summary>
        /// <param name="agent">a piece</param>
        /// <param name="position">a coordinate</param>
        /// <returns>If the placement is successful</returns>
        public bool TryPlace(Agent agent, Vector2 position)
        {
            if (IsPositionEmpty(position))
            {
                // Can place.
                // Remove the agent from the board first.
                agent.Remove();
                // Place the agent to the new position.
                ForcePlace(agent, position);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Force place an agent at the position.
        /// If the position is not on the board, agent is not put onto the board.
        /// </summary>
        /// <param name="agent"></param>
        /// <param name="position"></param>
        public void ForcePlace(Agent agent, Vector2 position)
        {
            if (this[position] == null) agent.Position = null;
            else
            {
                agent.Position = position;
                this[position] = agent;
            }
        }

        public void Clear(Vector2 position)
        {
            // Remove the agent from the board at this position.
            var slot = this[position];
            if (slot != null && slot is Agent)
                ((Agent)slot).Position = null;

            this[position] = new Empty(this);

            // If out of bounds, nothing happens.
        }
    }
}
