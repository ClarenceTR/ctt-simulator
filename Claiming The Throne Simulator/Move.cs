﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Claiming_The_Throne_Simulator
{
    public class CTTMove
    {
        public byte Faction;
        public byte PieceType;
        public List<CTTAvailableMove> moves = new List<CTTAvailableMove>();
        public Vector2Int? StartPosition
        {
            get
            {
                if (moves.Count > 0) return moves[0].StartPosition;
                else return null;
            }
        }
        public Stack<Vector2Int> EndPositionSequence
        {
            // Top: first position; Bottom: Last position
            get
            {
                var endPoses = new Stack<Vector2Int>();
                for (int i = moves.Count - 1; i >= 0; i--)
                {
                    endPoses.Push(moves[i].EndPosition);
                }
                return endPoses;
            }
        }
        public CTTDirection ChargeDirection = CTTDirection.Invalid;
        public CTTMoveInfo MoveInfo = new CTTMoveInfo();

        public CTTAvailableMove LastMove => moves.Count > 0 ? moves.Last() : null;
        public int Count => moves.Count;

        public void AddMove(CTTAvailableMove move)
        {
            // ? Is it necessary to ask if start position == last time end position?

            // Record this move.
            moves.Add(move);

            UpdateMoveInfo();

            // If it's a charge, record charge direction.
            if (move.MoveType == CTTMoveType.Charge)
            {
                ChargeDirection = GameManager.GetDirection(ref move.StartPosition, ref move.EndPosition);
            }
        }

        public bool UndoMove()
        {
            // Can't undo when no moves done.
            if (moves.Count == 0) return false;

            // Get the last move.
            var lastMove = moves.Last();

            // Remove this move from the mocked move stack.
            moves.Remove(lastMove);

            // Update move info and related.
            // Recalculate the moveInfo.
            UpdateMoveInfo();

            // Reset charge direction if nolonger charged.
            if (!MoveInfo.Charged) ChargeDirection = CTTDirection.Invalid;

            return true;
        }

        void UpdateMoveInfo()
        {
            byte mi = 0x00;
            foreach (var m in moves)
                mi |= m.MoveType;
            MoveInfo.Info = new BitArray(new byte[] { mi });
        }

        //public bool IsInvalid() => (PieceType ^ 0xff) == 0;
    }

    public class CTTAvailableMove
    {
        bool m_invalid;
        public Vector2Int StartPosition;
        public Vector2Int EndPosition;
        public Dictionary<Vector2Int, Piece> PiecesRemoved;
        public byte MoveType;

        public CTTAvailableMove()
        {
            m_invalid = false;
            PiecesRemoved = new Dictionary<Vector2Int, Piece>();
        }

        public static CTTAvailableMove Invalid
        {
            get
            {
                return new CTTAvailableMove() { m_invalid = true, };
            }
        }

        public bool IsInvalid() => m_invalid == true;
        public void SetValidation(bool value) { m_invalid = !value; if (m_invalid) { EndPosition = new Vector2Int(); PiecesRemoved.Clear(); } }
    }

    static class CTTMoveType
    {
        public const byte Step = 0x01;
        public const byte Jump = 0x02;
        public const byte Charge = 0x04; // A special step
        public const byte BigJump = 0x08;
    }

    public class CTTMoveInfo
    {
        public BitArray Info = new BitArray(8, false); // [0] is the digit in ones place, [1] is the digit in tens place, ...

        //public void AddNewMove(byte moveType)
        //{
        //    MoveInfo.And(new BitArray(new byte[] { moveType }));
        //}

        public bool Stepped
        {
            get
            {
                return Info[0];
            }
        }

        public bool Jumped
        {
            get
            {
                return Info[1];
            }
        }

        public bool Charged
        {
            get
            {
                return Info[2];
            }
        }

        public bool BigJumped
        {
            get
            {
                return Info[3];
            }
        }
    }
}
