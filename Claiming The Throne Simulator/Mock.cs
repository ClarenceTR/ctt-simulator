﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Claiming_The_Throne_Simulator
{
    public class MockBoard
    {
        Dictionary<Vector2, Slot> m_locations;

        public MockBoard(BoardManager board)
        {
            foreach (var key in board.Board)
            {
                m_locations.Add(key, board[key]);
            }
        }

        List<Pawn> capturedPieces;
    }

    //public enum CTTMoveType
    //{
    //    Step = 0,
    //    Jump = 1,
    //    Charge = 2,
    //    Leap = 3,
    //}

    //public class CTTMove
    //{
    //    public Vector2 start;
    //    public Vector2 end;
    //    public Pawn removedPawn = null;
    //    public CTTMoveType type;

    //    public CTTMove Reverse()
    //    {
            
    //    }
    //}

    //public class Player
    //{
    //    protected CTTFaction m_faction;
    //    protected BoardManager m_board;
    //    protected Pawn m_selectedPawn;
    //    protected MockBoard m_mockBoard;
    //    bool m_mocked = false;
    //    Stack<CTTMove> m_moveStack;

    //    public Player(BoardManager board, CTTFaction faction)
    //    {
    //        m_board = board;
    //        m_faction = faction;
    //        m_selectedPawn = null;
    //        m_mockBoard = null;
    //        m_moveStack = new Stack<CTTMove>();
    //    }

    //    public CTTFaction Faction { get => m_faction; set => m_faction = value; }

    //    public bool Select(Vector2 position)
    //    {
    //        // If the piece at this position is not mine (or empty), don't start doing nothing and return false.
    //        Slot slot = m_board[position];
    //        if (!(slot is Pawn)) return false;

    //        // Get the piece and store.
    //        Pawn pawn = (Pawn)slot;
    //        m_selectedPawn = pawn;

    //        // Set up the mock board.
    //        if (m_mockBoard == null || m_mocked)
    //            // If first time setting up mockboard or mock board is modified.
    //            m_mockBoard = new MockBoard(m_board);
    //        // Else, use the same mock board.

    //        // Successful. Return true.
    //        return true;
    //    }

    //    public bool TryStep(CTTDirection direction)
    //    {
    //        // If not selected a piece yet, return false.
    //        // Otherwise, add to the move stack and perform the move.
    //        if (m_selectedPawn == null) return false;

    //        // Add to the move stack.
    //        if (m_selectedPawn.TryMove(direction))
    //        {
    //            // 
    //        }


    //        // Perform the move on the mock board.
    //    }

    //    public bool TryJump(CTTDirection direction)
    //    {
    //        // If not selected a piece yet, return false.
    //        // Otherwise, add to the move stack and perform the move.

    //        // Get a score for this move. (based on what you capture)

    //        // Try to perform the move on the mock board.
    //        // If fails, return false. 

    //        // Else, Add to the move stack. And return true.
    //    }

    //    public bool TryCharge(CTTDirection direction)
    //    {
    //        // If not selected a piece yet, return false.
    //        // If not a knight, return false.

    //        // If has already charged, return false

    //        // Get a score for this move. (based on what you capture)

    //        // Perform.

    //        // Add this move.

    //    }

    //    public void TryLeap(CTTDirection direction)
    //    {
    //        // If not selected a piece yet, return false.
    //        // If not a rook, return false.

    //        // Get a score for this move. (based on what you capture)

    //        // Try to perform. If fails, return false.

    //        // Else, add this move, and return true.
    //    }

    //    public void EndMove()
    //    {
    //        // Apply the mock board to the actual board.

    //    }
    //}
}
