﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.Reflection;
using System.Diagnostics;

namespace Claiming_The_Throne_Simulator
{
    class Program
    {
        static void Main(string[] args)
        {
            GameManager gm = new GameManager();
            gm.Main();
        }

        //static void TestCreateBoard2VisualInteractive()
        //{
        //    ConsoleKey key;
        //    Vector2Int cursorPos = new Vector2Int();
        //    int xMaxView = 59;
        //    int yMaxView = 19;
        //    int xMinLogic = -10;
        //    int xMaxLogic = 10;
        //    int yMinLogic = -7;
        //    int yMaxLogic = 12;

        //    CTTBoard board = new CTTBoard();
        //    board.Reset();
        //    Console.Clear();

        //    PaintBoard(board);
        //    var viewPos = LogicPositionToView(cursorPos, yMaxView);
        //    Console.SetCursorPosition(viewPos.x, viewPos.y);

        //    do
        //    {
        //        // Input.
        //        key = Console.ReadKey(true).Key;
        //        switch (key)
        //        {
        //            case ConsoleKey.LeftArrow:
        //            case ConsoleKey.A:
        //            {
        //                cursorPos.x = Mathf.Clamp(cursorPos.x - 1, xMinLogic, xMaxLogic);
        //                break;
        //            }
        //            case ConsoleKey.RightArrow:
        //            case ConsoleKey.D:
        //            {
        //                cursorPos.x = Mathf.Clamp(cursorPos.x + 1, xMinLogic, xMaxLogic);
        //                break;
        //            }
        //            case ConsoleKey.UpArrow:
        //            case ConsoleKey.W:
        //            case ConsoleKey.E:
        //            {
        //                cursorPos.y = Mathf.Clamp(cursorPos.y + 1, yMinLogic, yMaxLogic);
        //                break;
        //            }
        //            case ConsoleKey.Q:
        //            {
        //                cursorPos.x = Mathf.Clamp(cursorPos.x - 1, xMinLogic, xMaxLogic);
        //                cursorPos.y = Mathf.Clamp(cursorPos.y + 1, yMinLogic, yMaxLogic);
        //                break;
        //            }
        //            case ConsoleKey.DownArrow:
        //            case ConsoleKey.S:
        //            case ConsoleKey.Z:
        //            {
        //                cursorPos.y = Mathf.Clamp(cursorPos.y - 1, yMinLogic, yMaxLogic);
        //                break;
        //            }
        //            case ConsoleKey.C:
        //            {
        //                cursorPos.x = Mathf.Clamp(cursorPos.x + 1, xMinLogic, xMaxLogic);
        //                cursorPos.y = Mathf.Clamp(cursorPos.y - 1, yMinLogic, yMaxLogic);
        //                break;
        //            }
        //            case ConsoleKey.Escape:
        //            {
        //                break;
        //            }
        //        }

        //        // Update.
        //        viewPos = LogicPositionToView(cursorPos, yMaxView);
        //        Console.SetCursorPosition(viewPos.x, viewPos.y);
        //    }
        //    while (true);

        //    Console.ReadKey();
        //}

        static void TestCreateBoard2Visual()
        {
            CTTBoard board = new CTTBoard();
            board.Reset();
            VisualizeBoard2(board);
            Console.ReadKey();
        }
        static void VisualizeBoard2(CTTBoard board)
        {
            // x: -10 to 10 -> 0 to 20 + h(x)  f(x) = 2x + 17      [-3, 37]
            // y: -7 to 12 -> 0 to 19   g(x) = -x + 12
            // Space: 0 to 19 -> 19 to 0 h(x) = -g(x) + 19


            for (int y = 12, numSpace = 19; y >= -7; y--, numSpace--)
            {
                string line = string.Empty;
                for (int i = 0; i < numSpace; i++)
                {
                    line += ' ';
                }
                for (int x = -10; x <= 10; x++)
                {
                    string spotStr = "X ";
                    Piece p = board[x, y];
                    if (p.IsInvalid)
                        spotStr = "- ";
                    else if (p.IsEmpty)
                        spotStr = "0 ";
                    else
                    {
                        if (p.Faction == CTTFaction2.One)
                            spotStr = "1 ";
                        else if (p.Faction == CTTFaction2.Two)
                            spotStr = "2 ";
                    }
                    line += spotStr;
                }
                Console.WriteLine(line);
            }
        }


        #region Generation 1

        static void TestPlay()
        {
            // If blocked, jump. Otherwise, move.
            BoardManager board = new BoardManager();
            Pawn agent = new Pawn(board, new Vector2(0, 0), CTTFaction.One);
            Pawn block1 = new Pawn(board, CTTFaction.One); block1.TryPlace(new Vector2(1, 0));    // Go right should be blocked
            Pawn block2 = new Pawn(board, CTTFaction.Two); block2.TryPlace(new Vector2(1, -1));   // Go down right should be blocked
            Pawn block3 = new Pawn(board, CTTFaction.Two); block3.TryPlace(new Vector2(2, -2));   // Jump down right should be blocked

            Console.WriteLine($"Current Position: {agent.Position}");
            for (; ; )
            {
                var key = Console.ReadKey();
                bool moveResult = false;
                bool jumpResult = false;
                if (key.Key == ConsoleKey.A)
                {
                    moveResult = agent.TryMove(CTTDirection.Left);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Left);
                }
                else if (key.Key == ConsoleKey.D)
                {
                    moveResult = agent.TryMove(CTTDirection.Right);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Right);
                }
                else if (key.Key == ConsoleKey.Q)
                {
                    moveResult = agent.TryMove(CTTDirection.Upleft);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Upleft);
                }
                else if (key.Key == ConsoleKey.C)
                {
                    moveResult = agent.TryMove(CTTDirection.Downright);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Downright);

                }
                else if (key.Key == ConsoleKey.E)
                {
                    moveResult = agent.TryMove(CTTDirection.Upright);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Upright);

                }
                else if (key.Key == ConsoleKey.Z)
                {
                    moveResult = agent.TryMove(CTTDirection.Downleft);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Downleft);

                }
                else if (key.Key == ConsoleKey.P)
                {
                    Console.WriteLine();
                    VisualizeBoard(board, agent.Position.Value);
                    Console.WriteLine();
                    jumpResult = true;  // Just to not print "Can neither move or jump."
                }
                else
                    break;
                if (!jumpResult) Console.WriteLine("Can neither move or jump.");
                Console.WriteLine($"Current Position: {agent.Position}");
            }
            Console.WriteLine("Any key to continue...");
            Console.ReadKey();
        }

        static void TestJump()
        {
            // If blocked, jump. Otherwise, move.
            BoardManager board = new BoardManager();
            Agent agent = new Agent(board, new Vector2(0, 0));
            Agent block1 = new Agent(board); block1.TryPlace(new Vector2(1, 0));    // Go right should be blocked
            Agent block2 = new Agent(board); block2.TryPlace(new Vector2(1, -1));   // Go down right should be blocked
            Agent block3 = new Agent(board); block3.TryPlace(new Vector2(2, -2));   // Jump down right should be blocked

            Console.WriteLine($"Current Position: {agent.Position}");
            for (; ; )
            {
                var key = Console.ReadKey();
                bool moveResult = false;
                bool jumpResult = false;
                if (key.Key == ConsoleKey.A)
                {
                    moveResult = agent.TryMove(CTTDirection.Left);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Left);
                }
                else if (key.Key == ConsoleKey.D)
                {
                    moveResult = agent.TryMove(CTTDirection.Right);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Right);
                }
                else if (key.Key == ConsoleKey.Q)
                {
                    moveResult = agent.TryMove(CTTDirection.Upleft);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Upleft);
                }
                else if (key.Key == ConsoleKey.C)
                {
                    moveResult = agent.TryMove(CTTDirection.Downright);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Downright);

                }
                else if (key.Key == ConsoleKey.E)
                {
                    moveResult = agent.TryMove(CTTDirection.Upright);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Upright);

                }
                else if (key.Key == ConsoleKey.Z)
                {
                    moveResult = agent.TryMove(CTTDirection.Downleft);
                    if (!moveResult) jumpResult = agent.TryJump(CTTDirection.Downleft);

                }
                else if (key.Key == ConsoleKey.P)
                {
                    Console.WriteLine();
                    VisualizeBoard(board, agent.Position.Value);
                    Console.WriteLine();
                    jumpResult = true;  // Just to not print "Can neither move or jump."
                }
                else
                    break;
                if (!jumpResult) Console.WriteLine("Can neither move or jump.");
                Console.WriteLine($"Current Position: {agent.Position}");
            }
            Console.WriteLine("Any key to continue...");
            Console.ReadKey();
        }

        static void TestCreateBoardVisualWithPieces()
        {
            BoardManager boardManager = new BoardManager();
            var listP = new List<Vector2>();
            listP.AddRange(boardManager.DistRightFief);
            listP.AddRange(boardManager.DistRightSupport);
            listP.AddRange(boardManager.DistLeftFief);
            listP.AddRange(boardManager.DistLeftSupport);
            foreach (var pos in listP)
            {
                boardManager.TryPlace(new Agent(boardManager), pos);
            }

            VisualizeBoardRec(boardManager);
            Console.ReadKey();
        }

        static void TestMovementWhenBlocked()
        {
            BoardManager board = new BoardManager();
            Agent agent = new Agent(board); agent.TryPlace(new Vector2(0, 0));
            Agent block1 = new Agent(board); block1.TryPlace(new Vector2(1, 0));    // Go right should be blocked
            Agent block2 = new Agent(board); block2.TryPlace(new Vector2(1, -1));   // Go down right should be blocked

            Console.WriteLine($"Current Position: {agent.Position}");
            for (; ; )
            {
                var key = Console.ReadKey();
                bool moveResult = false;
                if (key.Key == ConsoleKey.A)
                    moveResult = agent.TryMove(CTTDirection.Left);
                else if (key.Key == ConsoleKey.D)
                    moveResult = agent.TryMove(CTTDirection.Right);
                else if (key.Key == ConsoleKey.Q)
                    moveResult = agent.TryMove(CTTDirection.Upleft);
                else if (key.Key == ConsoleKey.C)
                    moveResult = agent.TryMove(CTTDirection.Downright);
                else if (key.Key == ConsoleKey.E)
                    moveResult = agent.TryMove(CTTDirection.Upright);
                else if (key.Key == ConsoleKey.Z)
                    moveResult = agent.TryMove(CTTDirection.Downleft);
                else
                    break;
                if (!moveResult) Console.WriteLine("The position is blocked.");
                Console.WriteLine($"Current Position: {agent.Position}");
            }
            Console.WriteLine("Any key to continue...");
            Console.ReadKey();
        }

        static void TestPlacePieces()
        {
            // Place y=x && y=-x pieces.
            BoardManager board = new BoardManager();

            var listP = new List<Vector2>();
            listP.AddRange(board.DistRightFief);
            listP.AddRange(board.DistRightSupport);
            listP.AddRange(board.DistLeftFief);
            listP.AddRange(board.DistLeftSupport);
            foreach (var pos in listP)
            {
                board.TryPlace(new Agent(board), pos);
            }

            // Print the board.
            List<Vector2> list = new List<Vector2>(board.Board);
            list.Sort(delegate (Vector2 a, Vector2 b)
            {
                if ((int)(a.x) != (int)(b.x))
                    return (int)(a.x - b.x);
                else
                    return (int)(a.y - b.y);
            });

            foreach (var key in list)
            {
                var slot = board[key];
                var name = slot.GetType().Name;

                Console.WriteLine($"({key.x}, {key.y}): {name}");
            }

            Console.ReadKey();

        }

        static void TestDistricts()
        {
            BoardManager boardManager = new BoardManager();

            Console.WriteLine("DistLeftFief: ");
            foreach (var pos in boardManager.DistLeftFief)
                Console.WriteLine($"({pos.x}, {pos.y})");
            Console.WriteLine();

            Console.WriteLine("DistLeftSupport: ");
            foreach (var pos in boardManager.DistLeftSupport)
                Console.WriteLine($"({pos.x}, {pos.y})");
            Console.WriteLine();

            Console.WriteLine("DistRightFief: ");
            foreach (var pos in boardManager.DistRightFief)
                Console.WriteLine($"({pos.x}, {pos.y})");
            Console.WriteLine();

            Console.WriteLine("DistRightSupport: ");
            foreach (var pos in boardManager.DistRightSupport)
                Console.WriteLine($"({pos.x}, {pos.y})");
            Console.WriteLine();

            Console.WriteLine("DistCapital: ");
            foreach (var pos in boardManager.DistCapital)
                Console.WriteLine($"({pos.x}, {pos.y})");
            Console.WriteLine();

            Console.WriteLine("DistPalace: ");
            foreach (var pos in boardManager.DistPalace)
                Console.WriteLine($"({pos.x}, {pos.y})");
            Console.WriteLine();

            Console.ReadKey();
        }

        static void TestMovement()
        {
            BoardManager board = new BoardManager();
            Agent agent = new Agent(board, new Vector2(0, 0));
            Console.WriteLine($"Current Position: {agent.Position}");
            for (; ; )
            {
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.A)
                    agent.TryMove(CTTDirection.Left);
                else if (key.Key == ConsoleKey.D)
                    agent.TryMove(CTTDirection.Right);
                else if (key.Key == ConsoleKey.Q)
                    agent.TryMove(CTTDirection.Upleft);
                else if (key.Key == ConsoleKey.C)
                    agent.TryMove(CTTDirection.Downright);
                else if (key.Key == ConsoleKey.E)
                    agent.TryMove(CTTDirection.Upright);
                else if (key.Key == ConsoleKey.Z)
                    agent.TryMove(CTTDirection.Downleft);
                else
                    break;
                Console.WriteLine($"Current Position: {agent.Position}");
            }
            Console.WriteLine("Any key to continue...");
            Console.ReadKey();
        }

        static void TestCreateBoard()
        {
            BoardManager boardManager = new BoardManager();

            List<Vector2> list = new List<Vector2>(boardManager.Board);
            list.Sort(delegate (Vector2 a, Vector2 b)
            {
                if ((int)(a.x) != (int)(b.x))
                    return (int)(a.x - b.x);
                else
                    return (int)(a.y - b.y);
            });
            foreach (var item in list)
            {
                Console.WriteLine($"({item.x}, {item.y})");

            }
            Console.ReadKey();
        }

        static void TestCreateHexagon()
        {
            BoardManager boardManager = new BoardManager();

            for (; ; )
            {
                string input = Console.ReadLine();
                if (input == "q" || input == "Q" || input.ToLower() == "quit")
                    break;

                int radius;
                if (int.TryParse(input, out radius))
                {
                    var list = boardManager.CreateHexagon(radius);
                    foreach (var item in list)
                        Console.WriteLine($"({item.x}, {item.y})");
                }
                else
                {
                    Console.WriteLine("Please type something that makes sense.");
                }
            }
            Console.WriteLine("Type anything to quit.");
            Console.ReadKey();
        }

        // *************************************************************

        static void VisualizeBoard(BoardManager boardManager, Vector2 playerPos)
        {
            // x: -10 to 10
            // y: - 7 to 12
            for (int y = 12, numSpace = 19; y >= -7; y--, numSpace--)
            {
                string line = string.Empty;
                for (int i = 0; i < numSpace; i++)
                {
                    line += ' ';
                }
                for (int x = -10; x <= 10; x++)
                {
                    string spotStr = "X ";
                    Slot spot = boardManager[new Vector2(x, y)];
                    if (spot == null)
                        spotStr = "- ";
                    else if (spot is Empty)
                        spotStr = "0 ";
                    else if ((int)playerPos.x == x && (int)playerPos.y == y)
                        spotStr = "P "; // Player
                    else if (spot is Pawn)
                    {
                        Pawn pawn = (Pawn)spot;
                        if (pawn.Faction == CTTFaction.One)
                            spotStr = "1 ";
                        else if (pawn.Faction == CTTFaction.Two)
                            spotStr = "2 ";
                    }
                    line += spotStr;
                }
                Console.WriteLine(line);
            }
        }

        static void VisualizeBoardRec(BoardManager boardManager)
        {
            // x: -10 to 10
            // y: - 7 to 12
            for (int y = 12; y >= -7; y--)
            {
                string line = string.Empty;
                for (int x = -10; x <= 10; x++)
                {
                    string spotStr = "1 ";
                    Slot spot = boardManager[new Vector2(x, y)];
                    if (spot == null)
                        spotStr = "- ";
                    else if (spot is Empty)
                        spotStr = "0 ";

                    line += spotStr;
                }
                Console.WriteLine(line);
            }
        }

        static void VisualizeBoardNoFaction(BoardManager boardManager, Vector2 playerPos)
        {
            // x: -10 to 10
            // y: - 7 to 12
            for (int y = 12, numSpace = 19; y >= -7; y--, numSpace--)
            {
                string line = string.Empty;
                for (int i = 0; i < numSpace; i++)
                {
                    line += ' ';
                }
                for (int x = -10; x <= 10; x++)
                {
                    string spotStr = "1 ";
                    Slot spot = boardManager[new Vector2(x, y)];
                    if (spot == null)
                        spotStr = "- ";
                    else if (spot is Empty)
                        spotStr = "0 ";
                    else if ((int)playerPos.x == x && (int)playerPos.y == y)
                        spotStr = "P "; // Player

                    line += spotStr;
                }
                Console.WriteLine(line);
            }
        }

        #endregion

    }
}
