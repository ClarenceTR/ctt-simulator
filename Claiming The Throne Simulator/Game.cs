﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.Diagnostics;
using System.Threading;
using System.Media;
using Debug = System.Diagnostics.Debug;
using System.Collections;

namespace Claiming_The_Throne_Simulator
{
    public enum GameScene
    {
        MainMenu,
        Intro,
        Game,
    }

    public static class CTTGameStage
    {
        public const byte NothingSelected = 0x01;
        public const byte Started = 0x02;
        public const byte Displaying = 0x03;
    }

    public class GameStatus
    {
        public byte CurrentFaction;
        public byte GameStage;
        public int roundNumber = 1;

        public void NextPlayer()
        {
            if (CurrentFaction == CTTFaction2.One)
            {
                CurrentFaction = CTTFaction2.Two;
            }
            else
            {
                CurrentFaction = CTTFaction2.One;
                roundNumber += 1;
            }
            GameStage = CTTGameStage.NothingSelected;
        }
    }

    public enum CTTDirectionNoSelect
    {
        Normal,
        ChosenEmpty,
        ChosenInvalid,
        WrongFaction,
    }

    public enum CTTDirectionStarted
    {
        Normal,
        CantSelect,
        NoMore,
        SelectContinue,
    }

    public class GameManager
    {
        #region Members
        #region Game logic:
        Vector2Int m_cursorPos = new Vector2Int();
        Vector2Int m_prevCursorPos = new Vector2Int();
        ConsoleKey key;
        GameScene m_prevScene = GameScene.Game;
        GameScene m_currScene = GameScene.Game;
        Stopwatch stopWatch;
        double lastTimeElasped = 0;
        int m_xWindowSize = 56;
        int m_yWindowSize = 36;
        string m_title = "Claiming the Throne";
        string m_crown0 = @"o   ()   o";
        string m_crown1 = @"|\  /\  /|";
        string m_crown2 = @"| \/  \/ |";
        string m_crown3 = @"|________|";
        #endregion

        #region MainMenu Scene logic:
        string[] m_mm_options = new string[] { "Start", "Quit" };
        int m_mm_currOption = 0;
        int m_mm_xMenuStart = 0;
        int m_mm_yMenuStart = 6;
        int m_mm_optionsPerLine = 1;
        //int m_mm_spacingPerLine = 15;
        ConsoleColor[] m_mm_crownColors = new ConsoleColor[] {
            ConsoleColor.Black,
            ConsoleColor.DarkRed,
            ConsoleColor.Red,
            ConsoleColor.Yellow,
            ConsoleColor.DarkYellow,
            ConsoleColor.Yellow,
            ConsoleColor.Magenta,
            ConsoleColor.DarkMagenta,};
        int m_mm_currCrownColorIndex = 0;
        double m_mm_crownColorSwitchTimer = 0;
        double m_mm_crownColorSwitchCD = 0.4f;
        int m_mm_yCrownStart = 0;
        #endregion

        #region Intro Scene logic:
        string[] m_i_lines = new string[] {
            "You are a prince of a kingdom. The old king passed away\n but no heir has been selected yet. ",
            "You decided to rush to the palace from your fief to \ninherit the throne. But you know your brother who’s also\n a prince will do the same.",
            "Command your army to escort you to the palace safely.",
        };
        int m_i_currLineIndex = 0;
        bool m_i_renderContinue = false;
        string m_i_continueStr = "[F] or [Enter] to continue.";
        double m_i_typingTimer = 0;
        double m_i_typingCD = 0.05;
        bool m_i_typingAnimDuring = true;
        int m_i_xStart;
        int m_i_yStart;
        string m_i_currStr = string.Empty;
        int m_i_currTypeIndex = 0;
        int m_i_prevLineIndex = 0;
        bool m_i_skipTyping = false;
        #endregion

        #region Game Scene logic:
        Player m_g_player1;
        Player m_g_player2;
        CTTBoard board;
        GameStatus gameStatus;
        bool m_g_initialized;
        Dictionary<Vector2Int, CTTAvailableMove> m_g_availableMoves;

        // Blink cursor
        double m_g_cursorSwitchTimer = 0;
        double m_g_cursorSwitchCD = .8f;
        bool m_g_showCursor = true;

        // Last time cursor pos
        Vector2Int m_g_lastTimeCursorPos1 = new Vector2Int();
        Vector2Int m_g_lastTimeCursorPos2 = new Vector2Int();

        // Blink crown
        ConsoleColor[] m_g_crownColors1 = new ConsoleColor[] {
            ConsoleColor.Blue,
            ConsoleColor.DarkBlue,
            ConsoleColor.Cyan,
            ConsoleColor.DarkCyan,
            ConsoleColor.Yellow,
            ConsoleColor.DarkYellow,
        };
        ConsoleColor[] m_g_crownColors2 = new ConsoleColor[] {
            ConsoleColor.Red,
            ConsoleColor.DarkRed,
            ConsoleColor.Magenta,
            ConsoleColor.DarkMagenta,
            ConsoleColor.Yellow,
            ConsoleColor.DarkYellow,
        };
        int m_g_currCrownColorIndex = 0;
        double m_g_crownColorSwitchTimer = 0;
        double m_g_crownColorSwitchCD = 1.6f;
        int m_g_yCrownStart = 0;

        // Blink available positions
        double m_g_availablePosBlinkTimer = 0;
        double m_g_availablePosBlinkCD = .8;
        bool m_g_availablePosBlinkVisible = true;

        // Round info
        int m_g_xRoundInfoStart = 0;
        int m_g_yRoundInfoStart = 5;
        int m_g_yCursorInfoStart = 34;

        // Direction
        int m_g_xDirectionStart = 0;
        int m_g_yDirectionStart = 29;

        // Board position
        int m_xBoardStart = 0;
        int m_yBoardStart = 9;

        // Render info
        bool m_g_boardNeedsRepaint = true;
        bool m_g_roundInfoNeedsReprint = true;
        bool m_g_cursorInfoNeedsReprint = true;
        bool m_g_crownNeedsRepaint = true;
        bool m_g_directionNeedsReprint = true;
        CTTDirectionNoSelect m_g_directionNothingSelected = CTTDirectionNoSelect.Normal;
        CTTDirectionStarted m_g_directionNothingStarted = CTTDirectionStarted.Normal;

        // Displaying player move (during Displaying stage)
        double m_g_applyingTimer = 0;
        double m_g_applyingCD = 1;
        Stack<CTTAvailableMove> m_g_moveStack = new Stack<CTTAvailableMove>();
        #endregion

        #region Board logic:
        int m_xMinView /*= xBoartStart*/;
        int m_yMinView /*= yBoardStart*/;
        int m_xMaxView /*= 59 + xBoartStart*/;
        int m_yMaxView /*= 19 + yBoardStart*/;
        int m_xMinLogic = -10;
        int m_xMaxLogic = 10;
        int m_yMinLogic = -7;
        int m_yMaxLogic = 12;
        #endregion
        #endregion

        public GameManager()
        {
            // Locally initialize.
            m_xMinView = m_xBoardStart;
            m_yMinView = m_yBoardStart;
            m_xMaxView = 59 + m_xBoardStart;
            m_yMaxView = 19 + m_yBoardStart;
            stopWatch = new Stopwatch();

            // Set window size.
            Console.SetWindowSize(120, m_yWindowSize);
        }

        public void Main()
        {
            #region Initialize
            // Setup deltaTime.
            stopWatch.Start();
            lastTimeElasped = stopWatch.Elapsed.TotalSeconds;

            // Scene initialization.
            SwitchSceneTo(m_currScene);
            #endregion

            // Start the input detection asynchronously.
            Task t_handleInput = new Task(HandleInput);
            t_handleInput.Start();

            // Update loop
            do
            {
                Update();
            }
            while (true);
        }

        #region Main Game loop

        void HandleInput()
        {
            do
            {
                switch (m_currScene)
                {
                    case GameScene.MainMenu:
                        MM_HandleInput();
                        break;
                    case GameScene.Intro:
                        I_HandleInput();
                        break;
                    case GameScene.Game:
                        G_HandleInput();
                        break;
                    default:
                        break;
                }
            } while (true);
        }
        void Update()
        {
            // Game anyway.
            // Timer dependant.
            // Timer related update.
            double deltaTime = HandleTime(stopWatch, ref lastTimeElasped);

            // Scenes.
            if (m_prevScene != m_currScene)
            {
                // Scene transition.
                Console.Clear();

                m_prevScene = m_currScene;
            }
            switch (m_currScene)
            {
                case GameScene.MainMenu:
                    MM_Update(deltaTime);
                    break;
                case GameScene.Intro:
                    I_Update(deltaTime);
                    break;
                case GameScene.Game:
                    G_Update(deltaTime);
                    break;
                default:
                    break;
            }
        }

        public void MM_HandleInput()
        {
            // Input.
            key = Console.ReadKey(true).Key;
            switch (key)
            {
                case ConsoleKey.LeftArrow:
                case ConsoleKey.A:
                {
                    if (m_mm_currOption % m_mm_optionsPerLine > 0)
                        m_mm_currOption -= 1;

                    // Play sound.
                    Sound.PlayOneShot(Sound.BtnSelPath);
                    break;
                }
                case ConsoleKey.RightArrow:
                case ConsoleKey.D:
                {
                    if (m_mm_currOption % m_mm_optionsPerLine < m_mm_optionsPerLine - 1)
                        m_mm_currOption += 1;
                    Sound.PlayOneShot(Sound.BtnSelPath);
                    break;
                }
                case ConsoleKey.UpArrow:
                case ConsoleKey.W:
                {
                    if (m_mm_currOption >= m_mm_optionsPerLine)
                        m_mm_currOption -= m_mm_optionsPerLine;
                    Sound.PlayOneShot(Sound.BtnSelPath);
                    break;
                }
                case ConsoleKey.DownArrow:
                case ConsoleKey.S:
                {
                    if (m_mm_currOption + m_mm_optionsPerLine < m_mm_options.Length)
                        m_mm_currOption += m_mm_optionsPerLine;
                    Sound.PlayOneShot(Sound.BtnSelPath);
                    break;
                }
                case ConsoleKey.Enter:
                case ConsoleKey.F:
                {
                    // Play the click sound.
                    Sound.PlayOneShot(Sound.SceneSwitchPath, true);

                    // Select the current option.
                    string currO = m_mm_options[m_mm_currOption];
                    switch (currO)
                    {
                        case "Start":
                            SwitchSceneTo(GameScene.Intro);
                            break;
                        case "Quit":
                            Environment.Exit(0);
                            break;
                        default:
                            break;
                    }
                    break;
                }
            }
        }
        public void MM_Update(double deltaTime)
        {
            // Hide Cursor.
            Console.CursorVisible = false;

            // Timer.
            m_mm_crownColorSwitchTimer -= deltaTime;
            if (m_mm_crownColorSwitchTimer < 0)
            {
                // Switch color.
                m_mm_currCrownColorIndex = (m_mm_currCrownColorIndex + 1) % m_mm_crownColors.Length;
                m_mm_crownColorSwitchTimer = m_mm_crownColorSwitchCD;
            }

            // Render.
            // Crown.
            PaintCrown(m_mm_yCrownStart, m_mm_crownColors[m_mm_currCrownColorIndex]);
            // Title.
            int titleStartPos = MiddleAlign(m_title, m_xWindowSize);
            Console.SetCursorPosition(titleStartPos, 4);
            Console.Write(m_title);

            // Menu.
            for (int i = 0; i < m_mm_options.Length; i++)
            {
                // Display like a table.
                //Console.SetCursorPosition(m_mm_xMenuStart + (i % m_mm_optionsPerLine) * m_mm_spacingPerLine, m_mm_yMenuStart + i / m_mm_optionsPerLine);

                // Display line by line.
                Console.SetCursorPosition(m_mm_xMenuStart + MiddleAlign(m_mm_options[i], m_xWindowSize), m_mm_yMenuStart + i);

                if (m_mm_currOption == i)
                    Console.ForegroundColor = ConsoleColor.Blue;

                Console.Write(m_mm_options[i]);
                Console.ResetColor();
            }
        }

        void I_HandleInput()
        {
            // If press [F] or [Enter], skip typing effect first, then go to the next line.
            // Input.
            key = Console.ReadKey(true).Key;
            switch (key)
            {
                case ConsoleKey.F:
                case ConsoleKey.Enter:
                {
                    if (m_i_typingAnimDuring)
                    {
                        // Skipping typing plays sound.
                        Sound.PlayOneShot(Sound.BtnSelPath, true);
                        m_i_skipTyping = true;
                    }
                    else
                    {
                        m_i_currLineIndex += 1;
                        if (m_i_currLineIndex >= m_i_lines.Length)
                        {
                            // Going to main scene plays sound.
                            Sound.PlayOneShot(Sound.SceneSwitchPath, true);

                            // Go to the main game scene.
                            SwitchSceneTo(GameScene.Game);
                        }
                    }

                    break;
                }
                case ConsoleKey.Escape:
                {
                    break;
                }
            }
        }
        void I_Update(double deltaTime)
        {
            // Logic.
            if (m_i_prevLineIndex != m_i_currLineIndex)
            {
                // Line changed.
                Console.Clear();
                // Display this next line.
                m_i_currStr = string.Empty;
                m_i_currTypeIndex = 0;
                m_i_typingAnimDuring = true;
                m_i_prevLineIndex = m_i_currLineIndex;
                m_i_skipTyping = false;
            }
            if (m_i_skipTyping)
            {
                SkipTyping();
                m_i_skipTyping = false;
            }
            m_i_renderContinue = !m_i_typingAnimDuring;

            // Timer.
            if (m_i_typingAnimDuring && m_i_currLineIndex < m_i_lines.Length)
            {
                m_i_typingTimer -= deltaTime;
                if (m_i_typingTimer < 0)
                {
                    var currLine = m_i_lines[m_i_currLineIndex];
                    if (m_i_currTypeIndex >= currLine.Length)
                    // Typing this line is over. Waiting for input to go next.
                    {
                        m_i_typingAnimDuring = false;
                    }
                    else
                    {
                        m_i_currStr += currLine[m_i_currTypeIndex];
                        m_i_currTypeIndex += 1;
                        m_i_typingTimer = m_i_typingCD;

                        // Also play sound.
                        //Sound.Stop();
                        Sound.PlayOneShot(Sound.KeyDownPath);
                    }
                }
            }

            // Render.
            Console.SetCursorPosition(m_i_xStart, m_i_yStart);
            // Render lines already rendered.
            for (int i = 0; i < m_i_currLineIndex; i++)
            {
                Console.WriteLine(m_i_lines[i]); Console.WriteLine();
            }

            // Render current stuff.
            Console.WriteLine(m_i_currStr); Console.WriteLine();

            // Render continue prompt.
            if (m_i_renderContinue)
                Console.WriteLine(m_i_continueStr);
        }

        void G_HandleInput()
        {
            // Input.
            key = Console.ReadKey(true).Key;
            switch (key)
            {
                case ConsoleKey.LeftArrow:
                case ConsoleKey.A:
                {
                    m_cursorPos.x = Mathf.Clamp(m_cursorPos.x - 1, m_xMinLogic, m_xMaxLogic);
                    Sound.PlayOneShot(Sound.NormalSelPath);
                    break;
                }
                case ConsoleKey.RightArrow:
                case ConsoleKey.D:
                {
                    m_cursorPos.x = Mathf.Clamp(m_cursorPos.x + 1, m_xMinLogic, m_xMaxLogic);
                    Sound.PlayOneShot(Sound.NormalSelPath);
                    break;
                }
                case ConsoleKey.UpArrow:
                case ConsoleKey.W:
                case ConsoleKey.E:
                {
                    m_cursorPos.y = Mathf.Clamp(m_cursorPos.y + 1, m_yMinLogic, m_yMaxLogic);
                    Sound.PlayOneShot(Sound.NormalSelPath);
                    break;
                }
                case ConsoleKey.Q:
                {
                    m_cursorPos.x = Mathf.Clamp(m_cursorPos.x - 1, m_xMinLogic, m_xMaxLogic);
                    m_cursorPos.y = Mathf.Clamp(m_cursorPos.y + 1, m_yMinLogic, m_yMaxLogic);
                    Sound.PlayOneShot(Sound.NormalSelPath);
                    break;
                }
                case ConsoleKey.DownArrow:
                case ConsoleKey.S:
                case ConsoleKey.Z:
                {
                    m_cursorPos.y = Mathf.Clamp(m_cursorPos.y - 1, m_yMinLogic, m_yMaxLogic);
                    Sound.PlayOneShot(Sound.NormalSelPath);
                    break;
                }
                case ConsoleKey.C:
                {
                    m_cursorPos.x = Mathf.Clamp(m_cursorPos.x + 1, m_xMinLogic, m_xMaxLogic);
                    m_cursorPos.y = Mathf.Clamp(m_cursorPos.y - 1, m_yMinLogic, m_yMaxLogic);
                    Sound.PlayOneShot(Sound.NormalSelPath);
                    break;
                }
                case ConsoleKey.F:
                case ConsoleKey.Enter:
                {
                    switch (gameStatus.GameStage)
                    {
                        case CTTGameStage.NothingSelected:
                        {
                            // Select the cursor at.
                            bool successful = CurrentPlayer.Select(m_cursorPos);
                            if (successful)
                            {
                                // Enter the mode.
                                gameStatus.GameStage = CTTGameStage.Started;

                                // Calculate the available moves.
                                m_g_availableMoves = CurrentPlayer.CalculateAvailable();

                                // Update direction.
                                m_g_directionNothingStarted = CTTDirectionStarted.Normal;
                            }
                            else
                            {
                                // Can't select here.
                                Piece piece = board[m_cursorPos];
                                if (piece.Type == CTTType.Empty)
                                    m_g_directionNothingSelected = CTTDirectionNoSelect.ChosenEmpty;
                                else if (piece.Type == CTTType.Null)
                                    m_g_directionNothingSelected = CTTDirectionNoSelect.ChosenInvalid;
                                else if (piece.Faction != gameStatus.CurrentFaction)
                                    m_g_directionNothingSelected = CTTDirectionNoSelect.WrongFaction;
                                else
                                    m_g_directionNothingSelected = CTTDirectionNoSelect.Normal;

                                // Play sound.
                                Sound.PlayOneShot(Sound.DeniedSelPath);
                            }
                            m_g_directionNeedsReprint = true;
                            break;
                        }
                        case CTTGameStage.Started:
                        {
                            // Is it one of the available moves?
                            if (m_g_availableMoves.Keys.Contains(m_cursorPos))
                            {
                                // Ok. Go here.
                                // Put this piece onto the mock board. 
                                CurrentPlayer.MockMove(m_cursorPos);

                                // Display the mock board.
                                m_g_boardNeedsRepaint = true;

                                // Display the new available positions.
                                m_g_availableMoves = CurrentPlayer.CalculateAvailable();

                                // Display direction.
                                if (m_g_availableMoves.Count > 0)
                                    m_g_directionNothingStarted = CTTDirectionStarted.SelectContinue;
                                else
                                    m_g_directionNothingStarted = CTTDirectionStarted.NoMore;
                            }
                            else
                            {
                                // Can't select here.
                                m_g_directionNothingStarted = CTTDirectionStarted.CantSelect;

                                // Play sound.
                                Sound.PlayOneShot(Sound.DeniedSelPath);
                            }
                            m_g_directionNeedsReprint = true;
                            break;
                        }
                        case CTTGameStage.Displaying:
                            // Skip the current choice display.
                            m_g_applyingTimer = 0;
                            break;
                    }
                    break;
                }
                case ConsoleKey.R:
                {
                    // Go back one step.
                    break;
                }
                case ConsoleKey.G:
                {
                    // Confirm the move and go to the next turn.
                    switch (gameStatus.GameStage)
                    {
                        case CTTGameStage.NothingSelected:
                        {
                            // You haven't selected anything! Can't move.
                            break;
                        }
                        case CTTGameStage.Started:
                        {
                            // If doens't have any move yet, can't confirm. Ignore.
                            // else, do this move.
                            if (CurrentPlayer.Moved)
                            {
                                EnterDisplayStage();
                            }

                            break;
                        }
                        default:
                            break;
                    }
                    break;
                }
                case ConsoleKey.Escape:
                {
                    switch (gameStatus.GameStage)
                    {
                        case CTTGameStage.Started:
                        {
                            // Quit selection.
                            // ? Is it necessary to clear the available positions when SelectNothing()? (Currently it's not cleared)
                            CurrentPlayer.SelectNothing();
                            gameStatus.GameStage = CTTGameStage.NothingSelected;
                            m_g_directionNeedsReprint = true;

                            // Set the direction no selection normal.
                            m_g_directionNothingSelected = CTTDirectionNoSelect.Normal;

                            // Hide the mock board of the current player. If the mock board is dirty, display the real board again.
                            if (CurrentPlayer.MockBoardDirty || !m_g_availablePosBlinkVisible)
                                m_g_boardNeedsRepaint = true;
                            break;
                        }
                        default:
                            break;
                    }

                    break;
                }
            }
        }

        public void G_Update(double deltaTime)
        {
            if (!m_g_initialized) return;

            // Timer.
            if (ShowCursor)
            {
                m_g_cursorSwitchTimer -= deltaTime;
                if (m_g_cursorSwitchTimer < 0)
                {
                    Console.CursorVisible = !Console.CursorVisible;
                    m_g_cursorSwitchTimer = m_g_cursorSwitchCD;
                }
            }

            m_g_crownColorSwitchTimer -= deltaTime;
            if (m_g_crownColorSwitchTimer < 0)
            {
                // Switch color.
                m_g_currCrownColorIndex = (m_g_currCrownColorIndex + 1) % m_g_crownColors1.Length;
                // Paint Crown.
                m_g_crownNeedsRepaint = true;

                m_g_crownColorSwitchTimer = m_g_crownColorSwitchCD;
            }

            m_g_availablePosBlinkTimer -= deltaTime;
            if (m_g_availablePosBlinkTimer < 0)
            {
                if (gameStatus.GameStage == CTTGameStage.Started)
                {
                    // Blink the available positions.
                    m_g_availablePosBlinkVisible = !m_g_availablePosBlinkVisible;
                    SetAvailablePosBlinkStatus(m_g_availablePosBlinkVisible);
                }

                m_g_availablePosBlinkTimer = m_g_availablePosBlinkCD;
            }

            if (gameStatus.GameStage == CTTGameStage.Displaying)
            {
                m_g_applyingTimer -= deltaTime;
                if (m_g_applyingTimer < 0)
                {
                    // Display a move.
                    if (m_g_moveStack.Count > 0)
                    {
                        var move = m_g_moveStack.Pop();
                        // Go to the end position on the real board.
                        board[move.EndPosition] = board[move.StartPosition];
                        board[move.StartPosition] = Piece.Empty;
                        // Remove the pieces on the real board.
                        foreach (var p in move.PiecesRemoved.Keys)
                        {
                            board[p] = Piece.Empty;
                        }

                        m_g_boardNeedsRepaint = true;

                        m_g_applyingTimer = m_g_applyingCD;

                        // Also play sound.
                        Sound.PlayOneShot(Sound.BtnSelPath);
                    }
                    else
                    {
                        // Display over.
                        DisplayStageOver();
                    }
                }
            }

            // Logic.

            // Render.
            // Crown.
            if (m_g_crownNeedsRepaint)
            {
                PaintCrown(m_g_yCrownStart, CrownColors[m_g_currCrownColorIndex]);
                m_g_crownNeedsRepaint = false;
            }
            // Round info.
            if (m_g_roundInfoNeedsReprint)
            {
                PrintRoundInfo();
                m_g_roundInfoNeedsReprint = false;
            }
            // Board.
            if (m_g_boardNeedsRepaint)
            {
                if (gameStatus.GameStage == CTTGameStage.Started)
                    PaintBoard(CurrentPlayer.MockBoard, m_xBoardStart, m_yBoardStart);
                else
                    PaintBoard(board, m_xBoardStart, m_yBoardStart);
                m_g_boardNeedsRepaint = false;
            }
            // Direction.
            if (m_g_directionNeedsReprint)
            {
                PrintDirection();
                m_g_directionNeedsReprint = false;
            }

            // Current cursor info.
            if (m_prevCursorPos != m_cursorPos)
            {
                m_g_cursorInfoNeedsReprint = true;
                m_prevCursorPos = m_cursorPos;
            }
            if (m_g_cursorInfoNeedsReprint)
            {
                PrintCursorInfo();
                m_g_cursorInfoNeedsReprint = false;
            }
            // TODO: Tips.

            // Update cursorPos
            var viewPos = LogicPositionToView(m_cursorPos, m_yMaxView, m_xBoardStart, m_yBoardStart);
            Console.SetCursorPosition(viewPos.x, viewPos.y);

            //Console.CursorVisible = true;
        }

        private void EnterDisplayStage()
        {
            // Record current player's last cursor position.
            LastTimeCursorPos = m_cursorPos;

            // Enter the display stage to show the animation of choices being applied. each step 1 second unless you press [F] to skip.
            m_g_applyingTimer = m_g_applyingCD;
            m_g_moveStack = CurrentPlayer.MoveStack;
            gameStatus.GameStage = CTTGameStage.Displaying;
            m_g_boardNeedsRepaint = true;   // To print the current state real board.
            m_g_directionNeedsReprint = true;   // To update the direction.

            // Hide the cursor when displaying.
            ShowCursor = false;
        }

        private void DisplayStageOver()
        {
            // Go to the next player.
            gameStatus.NextPlayer();
            m_g_roundInfoNeedsReprint = true;
            m_g_directionNeedsReprint = true;

            // Reset the crown color.
            m_g_currCrownColorIndex = 0;

            // Apply the last cursor position.
            m_cursorPos = LastTimeCursorPos;

            // Show cursor again.
            ShowCursor = true;
        }

        #endregion

        #region Printing Helpers

        void PrintCursorInfo()
        {
            Console.SetCursorPosition(m_g_xRoundInfoStart, m_g_yCursorInfoStart);
            Console.ResetColor();
            Console.Write($"Cursor at: ({m_cursorPos.x}, {m_cursorPos.y}), ");
            Piece cursorAt = board[m_cursorPos];
            ConsoleColor factionColor;
            if (cursorAt.Faction == CTTFaction2.One) factionColor = ConsoleColor.Blue;
            else if (cursorAt.Faction == CTTFaction2.Two) factionColor = ConsoleColor.Red;
            else factionColor = ConsoleColor.White;
            Console.ForegroundColor = factionColor;
            Console.Write(CTTType.ToName(cursorAt.Type));
            Console.ResetColor();
            Console.Write("          ");
        }
        void PrintRoundInfo()
        {
            string factionName = CurrentFactionName;
            ConsoleColor factionColor = CurrentFactionColor;
            string roundInfoAll = $"Round #: {gameStatus.roundNumber}, {factionName}'s turn.";
            Console.SetCursorPosition(MiddleAlign(roundInfoAll, m_xWindowSize), m_g_yRoundInfoStart);
            Console.ResetColor(); Console.Write($"Round #: ");
            Console.ForegroundColor = ConsoleColor.Cyan; Console.Write(gameStatus.roundNumber);
            Console.ResetColor(); Console.Write(", ");
            Console.ForegroundColor = factionColor; Console.Write(factionName);
            Console.ResetColor(); Console.Write("'s turn.");
        }
        void PaintBoard(CTTBoard board, int xOffset = 0, int yOffset = 0)
        {
            Console.SetCursorPosition(xOffset, yOffset);
            for (int y = 0; y <= 19; y++)
            {
                for (int x = 0; x <= 27; x++)
                {
                    if (y % 2 == 0)
                        Console.Write(" -");
                    else
                        Console.Write("- ");
                }
                Console.Write('\n');
            }

            // x: -10 to 10 -> 0 to 20 + h(x)  f(x) = 2x + 17      [-3, 37]
            // y: -7 to 12 -> 0 to 19   g(x) = -x + 12
            // Space: 0 to 19 -> 19 to 0 h(x) = -g(x) + 19
            for (int y = 12, numSpace = 19; y >= -7; y--, numSpace--)
                for (int x = -10; x <= 10; x++)
                {
                    int vx = f(x) + numSpace + xOffset;
                    int vy = g(y) + yOffset;
                    Console.SetCursorPosition(vx, vy);
                    Piece p = board[x, y];
                    if (p.IsInvalid)
                    {
                        if (y >= 9 && y >= -2 * x + 29)
                            Console.Write("  ");
                        else
                            Console.Write("- ");
                    }
                    else if (p.IsEmpty)
                        Console.Write("0 ");
                    else
                    {
                        if (p.Faction == CTTFaction2.One)
                            Console.ForegroundColor = ConsoleColor.Blue;
                        else if (p.Faction == CTTFaction2.Two)
                            Console.ForegroundColor = ConsoleColor.Red;
                        else
                            Console.ResetColor();

                        if (p.Type == CTTType.Pawn)
                            Console.Write("P ");
                        else if (p.Type == CTTType.Knight)
                            Console.Write("C ");
                        else if (p.Type == CTTType.Rook)
                            Console.Write("R ");
                        else if (p.Type == CTTType.Prince)
                            Console.Write("A ");
                        else
                            Console.Write("  ");

                        Console.ResetColor();
                    }
                }
        }
        void PaintCrown(int yStart, ConsoleColor color)
        {
            //Console.ForegroundColor = m_g_crownColors[m_g_currCrownColorIndex];
            Console.ForegroundColor = color;
            int crownStartPos = MiddleAlign(m_crown0, m_xWindowSize);
            Console.SetCursorPosition(crownStartPos, yStart + 0); Console.Write(m_crown0);
            Console.SetCursorPosition(crownStartPos, yStart + 1); Console.Write(m_crown1);
            Console.SetCursorPosition(crownStartPos, yStart + 2); Console.Write(m_crown2);
            Console.SetCursorPosition(crownStartPos, yStart + 3); Console.Write(m_crown3);
            Console.ResetColor();
        }
        void PrintDirection()
        {
            string blankFill = "                       ";

            if (gameStatus.GameStage == CTTGameStage.NothingSelected)
            {
                Console.ResetColor();
                switch (m_g_directionNothingSelected)
                {
                    case CTTDirectionNoSelect.Normal:
                    {
                        // Second line: [F] to select a piece of [your faction].
                        // Third line: [Esc] to quit to main menu.
                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 1);
                        Console.Write("[F] to select a piece of ");
                        Console.ForegroundColor = CurrentFactionColor;
                        Console.Write("your faction");
                        Console.ResetColor();
                        Console.Write(".");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 2);
                        Console.Write("[Esc] to quit to main menu.");
                        Console.Write(blankFill);
                        break;
                    }
                    case CTTDirectionNoSelect.ChosenEmpty:
                    {
                        // First line: It's empty.
                        // Second line: [F] to select a piece of [your faction].
                        // Third line: [Esc] to quit to main menu.
                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart);
                        Console.Write("It's ");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write("empty ");
                        Console.ResetColor();
                        Console.Write(".");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 1);
                        Console.Write("[F] to select a piece of ");
                        Console.ForegroundColor = CurrentFactionColor;
                        Console.Write("your faction");
                        Console.ResetColor();
                        Console.Write(".");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 2);
                        Console.Write("[Esc] to quit to main menu.");
                        Console.Write(blankFill);
                        break;
                    }
                    case CTTDirectionNoSelect.ChosenInvalid:
                    {
                        // First line: It's empty and out of bound.
                        // Second line: [F] to select a piece of [your faction].
                        // Third line: [Esc] to quit to main menu.
                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart);
                        Console.Write("It's ");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write("empty ");
                        Console.ResetColor();
                        Console.Write("and ");
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.Write("out of bound");
                        Console.ResetColor();
                        Console.Write(".");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 1);
                        Console.Write("[F] to select a piece of ");
                        Console.ForegroundColor = CurrentFactionColor;
                        Console.Write("your faction");
                        Console.ResetColor();
                        Console.Write(".");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 2);
                        Console.Write("[Esc] to quit to main menu.");
                        Console.Write(blankFill);
                        break;
                    }
                    case CTTDirectionNoSelect.WrongFaction:
                    {
                        // First line: You can't command your enemy.
                        // Second line: [F] to select a piece of [your faction].
                        // Third line: [Esc] to quit to main menu.
                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart);
                        Console.Write("You can't command your ");
                        Console.ForegroundColor = EnemyColor;
                        Console.Write("enemy");
                        Console.ResetColor();
                        Console.Write(".");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 1);
                        Console.Write("[F] to select a piece of ");
                        Console.ForegroundColor = CurrentFactionColor;
                        Console.Write("your faction");
                        Console.ResetColor();
                        Console.Write(".");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 2);
                        Console.Write("[Esc] to quit to main menu.");
                        Console.Write(blankFill);
                        break;
                    }
                    default:
                        break;
                }
            }
            else if (gameStatus.GameStage == CTTGameStage.Started)
            {
                Console.ResetColor();
                switch (m_g_directionNothingStarted)
                {
                    case CTTDirectionStarted.Normal:
                    {
                        // Second line: [F] to choose an available position.
                        // Third line: [Esc] or [R] to cancel.
                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 1);
                        Console.Write("[F] to choose an available position.");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 2);
                        Console.Write("[Esc] or [R] to cancel.");
                        Console.Write(blankFill);
                        break;
                    }
                    case CTTDirectionStarted.CantSelect:
                    {
                        // First line: Can't go there.
                        // Second line: [F] to select a piece of [your faction].
                        // Third line: [Esc] to quit to main menu.
                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Can't go there.");
                        Console.ResetColor();
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 1);
                        Console.Write("[F] to choose an available position.");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 2);
                        Console.Write("[Esc] or [R] to cancel.");
                        Console.Write(blankFill);
                        break;
                    }
                    case CTTDirectionStarted.SelectContinue:
                    {
                        // First line: [G] to confirm the move.
                        // Second line: [F] to continue moving.
                        // Third line: [Esc] or [R] to cancel.
                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart);
                        Console.Write("[G] to confirm the move.");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 1);
                        Console.Write("[F] to keep going.");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 2);
                        Console.Write("[Esc] or [R] to cancel.");
                        Console.Write(blankFill);
                        break;
                    }
                    case CTTDirectionStarted.NoMore:
                    {
                        // First line: No more possible positions to go to.
                        // Second line: [G] to confirm the move.
                        // Third line: [Esc] or [R] to cancel.
                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart);
                        Console.Write("No more possible positions to go to.");
                        Console.ResetColor();
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 1);
                        Console.Write("[G] to confirm the move.");
                        Console.Write(blankFill);

                        Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 2);
                        Console.Write("[Esc] or [R] to cancel.");
                        Console.Write(blankFill);
                        break;
                    }

                    default:
                        break;
                }
            }
            else if (gameStatus.GameStage == CTTGameStage.Displaying)
            {
                // Wipe out Line 1 to 3. 
                Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart);
                Console.Write(blankFill);
                Console.Write(blankFill);

                Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 1);
                Console.Write(blankFill);
                Console.Write(blankFill);

                Console.SetCursorPosition(m_g_xDirectionStart, m_g_yDirectionStart + 2);
                Console.Write(blankFill);
                Console.Write(blankFill);
            }
        }

        #endregion

        #region Logic Helpers

        static double HandleTime(Stopwatch stopWatch, ref double lastTimeElasped)
        {
            var thisTimeElapsed = stopWatch.Elapsed.TotalSeconds;
            var deltaTime = thisTimeElapsed - lastTimeElasped;
            lastTimeElasped = thisTimeElapsed;
            return deltaTime;
        }
        static int f(int x) => 2 * x + 20;  // x (- [-10, 10]   f(x)+numSpace (- [0, 59] 
        static int g(int y) => -y + 12;     // y (- [-7, 12]    g(x) (- [0, 19]
        static Vector2Int LogicPositionToView(Vector2Int logicPosition, int yMaxView, int xBoardStart = 0, int yBoardStart = 0)
        {
            return new Vector2Int(f(logicPosition.x) + -g(logicPosition.y) + yMaxView + xBoardStart - yBoardStart, g(logicPosition.y) + yBoardStart);
        }
        static int MiddleAlign(string str, int xWindowSize)
        {
            int len = str.Length;
            return (xWindowSize - len) / 2;
        }
        void SkipTyping()
        {
            m_i_currStr = m_i_lines[m_i_currLineIndex];
            m_i_typingAnimDuring = false;
            Console.Clear();
        }
        bool ShowCursor { get => m_g_showCursor; set { m_g_showCursor = Console.CursorVisible = value; if (value) m_g_cursorSwitchTimer = m_g_cursorSwitchCD; } }
        void SetAvailablePosBlinkStatus(bool visible)
        {
            string token = visible ? "0 " : "  ";
            foreach (var pos in m_g_availableMoves.Keys)
            {
                var vp = LogicPositionToView(pos, m_yMaxView, m_xBoardStart, m_yBoardStart);
                Console.SetCursorPosition(vp.x, vp.y);
                Console.ResetColor();
                Console.Write(token);
            }
        }

        #endregion

        #region Scene Initializer
        void SwitchSceneTo(GameScene scene)
        {
            m_currScene = scene;

            // Scene initialization.
            switch (scene)
            {
                case GameScene.MainMenu:

                    break;
                case GameScene.Intro:
                    m_i_currStr = string.Empty;
                    m_i_currLineIndex = 0;
                    m_i_currTypeIndex = 0;
                    m_i_typingAnimDuring = true;
                    m_i_prevLineIndex = m_i_currLineIndex;
                    m_i_skipTyping = false;
                    break;
                case GameScene.Game:
                    m_g_initialized = false;

                    // Setup board.
                    board = new CTTBoard();
                    board.Reset();
                    // Setup players.
                    m_g_player1 = new Player(board, CTTFaction2.One);
                    m_g_player2 = new Player(board, CTTFaction2.Two);
                    // Setup game status.
                    gameStatus = new GameStatus()
                    {
                        CurrentFaction = CTTFaction2.One,
                        GameStage = CTTGameStage.NothingSelected,
                        roundNumber = 1,
                    };
                    m_g_lastTimeCursorPos1 = new Vector2Int();
                    m_g_lastTimeCursorPos2 = new Vector2Int();
                    m_g_boardNeedsRepaint = true;
                    m_g_roundInfoNeedsReprint = true;
                    m_g_cursorInfoNeedsReprint = true;
                    m_g_directionNeedsReprint = true;
                    m_g_crownNeedsRepaint = true;
                    m_g_directionNothingSelected = CTTDirectionNoSelect.Normal;
                    m_g_directionNothingStarted = CTTDirectionStarted.Normal;
                    // Available moves.
                    m_g_availableMoves = new Dictionary<Vector2Int, CTTAvailableMove>();
                    // Initialize logic cursor position.
                    m_cursorPos = new Vector2Int();
                    // Display cursor (blinking).
                    m_g_showCursor = true;

                    m_g_initialized = true;
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region public (useful) properties

        public Player CurrentPlayer
        {
            get
            {
                if (gameStatus.CurrentFaction == CTTFaction2.One)
                    return m_g_player1;
                else
                    return m_g_player2;
            }
        }
        public string CurrentFactionName => gameStatus.CurrentFaction == CTTFaction2.One ? "Blue" : "Red";
        public ConsoleColor CurrentFactionColor => gameStatus.CurrentFaction == CTTFaction2.One ? ConsoleColor.Blue : ConsoleColor.Red;
        public ConsoleColor EnemyColor => gameStatus.CurrentFaction == CTTFaction2.One ? ConsoleColor.Red : ConsoleColor.Blue;
        public ConsoleColor[] CrownColors => gameStatus.CurrentFaction == CTTFaction2.One ? m_g_crownColors1 : m_g_crownColors2;
        public Vector2Int LastTimeCursorPos
        {
            get=> gameStatus.CurrentFaction == CTTFaction2.One ? m_g_lastTimeCursorPos1 : m_g_lastTimeCursorPos2;
            set { if (gameStatus.CurrentFaction == CTTFaction2.One) m_g_lastTimeCursorPos1 = value; else m_g_lastTimeCursorPos2 = value; }
        }

        #endregion

        #region public Static Methods

        public static Dictionary<Vector2Int, CTTAvailableMove> CalculateAvailable(Vector2Int position, CTTBoard board, CTTMove currentMove)
        {
            Dictionary<Vector2Int, CTTAvailableMove> res = new Dictionary<Vector2Int, CTTAvailableMove>();

            var piece = board[position];
            // Empty or out of bound renders this action invalid. No available moves.
            if (piece.IsEmpty || piece.IsInvalid) return res;

            // If charged during the current move, can only go the certain direction.


            // If stepped, unable to do anything else.
            if (currentMove.MoveInfo.Stepped) return res;

            Vector2Int
                right1 = position + Vector2Int.right,
                right2 = position + new Vector2Int(2, 0),
                upRight1 = position + Vector2Int.up,
                upRight2 = position + new Vector2Int(0, 2),
                upLeft1 = position + new Vector2Int(-1, 1),
                upLeft2 = position + new Vector2Int(-2, 2),
                left1 = position + Vector2Int.left,
                left2 = position + new Vector2Int(-2, 0),
                downLeft1 = position + Vector2Int.down,
                downLeft2 = position + new Vector2Int(0, -2),
                downRight1 = position + new Vector2Int(1, -1),
                downRight2 = position + new Vector2Int(2, -2);
            Piece
                right1Piece = board[right1],
                right2Piece = board[right2],
                upRight1Piece = board[upRight1],
                upRight2Piece = board[upRight2],
                upLeft1Piece = board[upLeft1],
                upLeft2Piece = board[upLeft2],
                left1Piece = board[left1],
                left2Piece = board[left2],
                downLeft1Piece = board[downLeft1],
                downLeft2Piece = board[downLeft2],
                downRight1Piece = board[downRight1],
                downRight2Piece = board[downRight2];

            CTTAvailableMove move;
            // Check step.
            // If jumped, can't step.
            if (!currentMove.MoveInfo.Jumped)
            {
                if (TryStep(ref position, ref right1, ref piece, ref right1Piece, out move)) res.Add(move.EndPosition, move);
                if (TryStep(ref position, ref upRight1, ref piece, ref upRight1Piece, out move)) res.Add(move.EndPosition, move);
                if (TryStep(ref position, ref upLeft1, ref piece, ref upLeft1Piece, out move)) res.Add(move.EndPosition, move);
                if (TryStep(ref position, ref left1, ref piece, ref left1Piece, out move)) res.Add(move.EndPosition, move);
                if (TryStep(ref position, ref downLeft1, ref piece, ref downLeft1Piece, out move)) res.Add(move.EndPosition, move);
                if (TryStep(ref position, ref downRight1, ref piece, ref downRight1Piece, out move)) res.Add(move.EndPosition, move);
            }

            bool jumpCheck = true;
            // Check charge.
            //If it's a knight.
            if (piece.Type == CTTType.Knight)
            {
                // If it has not charged but jumped, can both charge and jump.
                bool jumped = currentMove.MoveInfo.Jumped;
                bool charged = currentMove.MoveInfo.Charged;
                if (jumped && !charged)
                {
                    // Get last move's direction.
                    var lm = currentMove.LastMove;
                    Debug.Assert(lm != null);
                    CTTDirection dir = GetDirection(ref lm.StartPosition, ref lm.EndPosition);

                    // Add the charge (a special "step") available position 
                    switch (dir)
                    {
                        case CTTDirection.Right:
                            if (TryStep(ref position, ref right1, ref piece, ref right1Piece, out move)) { move.MoveType = CTTMoveType.Charge; res.Add(move.EndPosition, move); };
                            break;
                        case CTTDirection.Upright:
                            if (TryStep(ref position, ref upRight1, ref piece, ref upRight1Piece, out move)) { move.MoveType = CTTMoveType.Charge; res.Add(move.EndPosition, move); };
                            break;
                        case CTTDirection.Upleft:
                            if (TryStep(ref position, ref upLeft1, ref piece, ref upLeft1Piece, out move)) { move.MoveType = CTTMoveType.Charge; res.Add(move.EndPosition, move); };
                            break;
                        case CTTDirection.Left:
                            if (TryStep(ref position, ref left1, ref piece, ref left1Piece, out move)) { move.MoveType = CTTMoveType.Charge; res.Add(move.EndPosition, move); };
                            break;
                        case CTTDirection.Downleft:
                            if (TryStep(ref position, ref downLeft1, ref piece, ref downLeft1Piece, out move)) { move.MoveType = CTTMoveType.Charge; res.Add(move.EndPosition, move); };
                            break;
                        case CTTDirection.Downright:
                            if (TryStep(ref position, ref downRight1, ref piece, ref downRight1Piece, out move)) { move.MoveType = CTTMoveType.Charge; res.Add(move.EndPosition, move); };
                            break;
                        case CTTDirection.Invalid:
                            break;
                        default:
                            break;
                    }

                    // Still can jump.
                }
                // Else if it has both jumped and charged, can jump in the same direction.
                else if (jumped && charged)
                {
                    Debug.Assert(currentMove.ChargeDirection != CTTDirection.Invalid);
                    switch (currentMove.ChargeDirection)
                    {
                        case CTTDirection.Right:
                            if (TryJump(ref position, ref right1, ref right2, ref piece, ref right1Piece, ref right2Piece, out move)) res.Add(move.EndPosition, move);
                            break;
                        case CTTDirection.Upright:
                            if (TryJump(ref position, ref upRight1, ref upRight2, ref piece, ref upRight1Piece, ref upRight2Piece, out move)) res.Add(move.EndPosition, move);
                            break;
                        case CTTDirection.Upleft:
                            if (TryJump(ref position, ref upLeft1, ref upLeft2, ref piece, ref upLeft1Piece, ref upLeft2Piece, out move)) res.Add(move.EndPosition, move);
                            break;
                        case CTTDirection.Left:
                            if (TryJump(ref position, ref left1, ref left2, ref piece, ref left1Piece, ref left2Piece, out move)) res.Add(move.EndPosition, move);
                            break;
                        case CTTDirection.Downleft:
                            if (TryJump(ref position, ref downLeft1, ref downLeft2, ref piece, ref downLeft1Piece, ref downLeft2Piece, out move)) res.Add(move.EndPosition, move);
                            break;
                        case CTTDirection.Downright:
                            if (TryJump(ref position, ref downRight1, ref downRight2, ref piece, ref downRight1Piece, ref downRight2Piece, out move)) res.Add(move.EndPosition, move);
                            break;
                        case CTTDirection.Invalid:
                            break;
                        default:
                            break;
                    }

                    // Jump is done. Don't goto jump logic.
                    jumpCheck = false;
                }
                // Else it's impossible to have charged but not jumped.
                // Else, it's the same as the rest of the pieces.
                else
                {
                    Debug.Assert(!(charged && !jumped));
                    // Still can jump.
                }
            }

            if (jumpCheck)
            {
                // Can jump in all directions.
                if (TryJump(ref position, ref right1, ref right2, ref piece, ref right1Piece, ref right2Piece, out move)) res.Add(move.EndPosition, move);
                if (TryJump(ref position, ref upRight1, ref upRight2, ref piece, ref upRight1Piece, ref upRight2Piece, out move)) res.Add(move.EndPosition, move);
                if (TryJump(ref position, ref upLeft1, ref upLeft2, ref piece, ref upLeft1Piece, ref upLeft2Piece, out move)) res.Add(move.EndPosition, move);
                if (TryJump(ref position, ref left1, ref left2, ref piece, ref left1Piece, ref left2Piece, out move)) res.Add(move.EndPosition, move);
                if (TryJump(ref position, ref downLeft1, ref downLeft2, ref piece, ref downLeft1Piece, ref downLeft2Piece, out move)) res.Add(move.EndPosition, move);
                if (TryJump(ref position, ref downRight1, ref downRight2, ref piece, ref downRight1Piece, ref downRight2Piece, out move)) res.Add(move.EndPosition, move);
            }

            if (piece.Type == CTTType.Rook)
            {
                // It also needs to check big steps.
                Vector2Int
                      right3 = position + new Vector2Int(3, 0),
                    upRight3 = position + new Vector2Int(0, 3),
                     upLeft3 = position + new Vector2Int(-3, 3),
                       left3 = position + new Vector2Int(-3, 0),
                   downLeft3 = position + new Vector2Int(0, -3),
                  downRight3 = position + new Vector2Int(3, -3);
                Piece
                    right3Piece = board[right3],
                    upRight3Piece = board[upRight3],
                    upLeft3Piece = board[upLeft3],
                    left3Piece = board[left3],
                    downLeft3Piece = board[downLeft3],
                    downRight3Piece = board[downRight3];

                if (TryBigJump(ref position, ref right1, ref right2, ref right3, ref piece, ref right1Piece, ref right2Piece, ref right3Piece, out move)) res.Add(move.EndPosition, move);
                if (TryBigJump(ref position, ref upRight1, ref upRight2, ref upRight3, ref piece, ref upRight1Piece, ref upRight2Piece, ref upRight3Piece, out move)) res.Add(move.EndPosition, move);
                if (TryBigJump(ref position, ref upLeft1, ref upLeft2, ref upLeft3, ref piece, ref upLeft1Piece, ref upLeft2Piece, ref upLeft3Piece, out move)) res.Add(move.EndPosition, move);
                if (TryBigJump(ref position, ref left1, ref left2, ref left3, ref piece, ref left1Piece, ref left2Piece, ref left3Piece, out move)) res.Add(move.EndPosition, move);
                if (TryBigJump(ref position, ref downLeft1, ref downLeft2, ref downLeft3, ref piece, ref downLeft1Piece, ref downLeft2Piece, ref downLeft3Piece, out move)) res.Add(move.EndPosition, move);
                if (TryBigJump(ref position, ref downRight1, ref downRight2, ref downRight3, ref piece, ref downRight1Piece, ref downRight2Piece, ref downRight3Piece, out move)) res.Add(move.EndPosition, move);
            }
            return res;
        }

        static bool TryJump(ref Vector2Int me, ref Vector2Int mid, ref Vector2Int dest, ref Piece meP, ref Piece midP, ref Piece destP,
              out CTTAvailableMove move)
        {
            move = CTTAvailableMove.Invalid;
            // if position next to is empty or out of bound, can't jump.
            if (midP.IsEmpty || midP.IsInvalid) return false;
            // if the destination is not empty or out of bound, can't jump.
            // TODO: out of bound can jump for the elite pieces, and is just temporarily removed.
            if (!destP.IsEmpty) return false;

            // Same faction doesn't capture.
            if (meP.Faction != midP.Faction)
            {
                // Not same. Capture.
                move.SetValidation(true);
                move.PiecesRemoved.Add(mid, midP);
            }
            // Either way, start/end position is the same.
            move.StartPosition = me;
            move.EndPosition = dest;
            move.MoveType = CTTMoveType.Jump;
            return true;
        }

        static bool TryStep(ref Vector2Int me, ref Vector2Int dest, ref Piece meP, ref Piece destP,
           out CTTAvailableMove move)
        {
            move = CTTAvailableMove.Invalid;
            // if position next to is not empty or out of bound, can't step.
            if (!destP.IsEmpty) return false;

            // Won't capture anything.
            move.SetValidation(true);
            move.StartPosition = me;
            move.EndPosition = dest;
            move.MoveType = CTTMoveType.Step;
            return true;
        }

        static bool TryBigJump(ref Vector2Int me, ref Vector2Int mid1, ref Vector2Int mid2, ref Vector2Int dest, ref Piece meP, ref Piece mid1P, ref Piece mid2P, ref Piece destP,
            out CTTAvailableMove move)
        {
            move = CTTAvailableMove.Invalid;

            // if mid2 is empty or out of bound, treat as TryJump.
            if (mid2P.IsEmpty || mid2P.IsInvalid)
                return TryJump(ref me, ref mid1, ref mid2, ref meP, ref mid1P, ref mid2P, out move);

            // if mid1 is empty or out of bound, can't jump.
            // Possible optimization (less flexible though): in current map, mid1P won't be and shouldn't be invalid if mid2 is neither empty or our of bound.
            if (mid1P.IsEmpty || mid1P.IsInvalid) return false;
            // if the destination is not empty or out of bound, can't jump.
            // TODO: out of bound can jump for the elite pieces, and is just temporarily removed.
            if (!destP.IsEmpty) return false;

            // Same faction doesn't capture.
            if (meP.Faction != mid1P.Faction)
            {
                // Not same. Capture.
                move.SetValidation(true);
                move.PiecesRemoved.Add(mid1, mid1P);
            }
            if (meP.Faction != mid2P.Faction)
            {
                // Not same. Capture.
                move.SetValidation(true);
                move.PiecesRemoved.Add(mid2, mid2P);
            }

            // Anyway, start/end position is dest.
            move.StartPosition = me;
            move.EndPosition = dest;
            move.MoveType = CTTMoveType.BigJump;
            return true;
        }

        static public CTTDirection GetDirection(ref Vector2Int start, ref Vector2Int end)
        {
            // Three axes: y=-x + c; x=c; y=c;
            // Thus, three slopes: -1, Infinite and 0.
            int k;
            if (end.x - start.x == 0)
            {
                // x=c.
                if (end.y > start.y) return CTTDirection.Upright;
                else if (end.y < start.y) return CTTDirection.Downleft;
                else /* Same point */return CTTDirection.Invalid;
            }
            else
            {
                k = (end.y - start.y) / (end.x - start.x);
                if (k == 0)
                {
                    // y=c
                    if (end.x > start.x) return CTTDirection.Right;
                    return CTTDirection.Left;
                    // At this point, same point should have already been filtered.
                }
                else if (k == -1)
                {
                    // y = -x + c
                    if (end.x > start.x) return CTTDirection.Downright;
                    return CTTDirection.Upleft;
                    // At this point, same point should have already been filtered.
                }
                else
                {
                    // Invalid.
                    return CTTDirection.Invalid;
                }
            }

        }

        #endregion

        //void ClearArea(int xStart, int yStart, int xEnd, int yEnd)
        //{
        //    for (int y = yStart; y <= yEnd; y++)
        //    {
        //        Console.SetCursorPosition(xStart, y);
        //        for (int x = xStart; x < xEnd; x++)
        //        {
        //            Console.Write(' ');
        //        }
        //    }
        //}

        //if (m_g_currFaction == 1)
        //{
        //    // Wait until player 1 is done.
        //    if (m_g_player1.EndTurn)
        //    {
        //        m_g_currFaction = 2;
        //        m_g_player1.EndTurn = false;
        //        m_g_player2.EndTurn = false;
        //    }
        //}
        //else
        //{
        //    // Wait until player 2 is done.
        //    if (m_g_player2.EndTurn)
        //    {
        //        m_g_currFaction = 1;
        //        m_g_player1.EndTurn = false;
        //        m_g_player2.EndTurn = false;
        //    }
        //}
    }
}
