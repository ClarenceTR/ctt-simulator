﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Claiming_The_Throne_Simulator
{
    public enum CTTFaction
    {
        One = 1,
        Two = 2,
        None = 0,
    }

    

    public class Pawn : Agent
    {
        CTTFaction m_faction;

        public Pawn(BoardManager board) : base(board)
        {
            m_faction = CTTFaction.None;
        }

        public Pawn(BoardManager board, CTTFaction faction) : base(board)
        {
            m_faction = faction;
        }

        public Pawn(BoardManager board, Vector2 position, CTTFaction faction) : base(board, position)
        {
            m_faction = faction;
        }

        public CTTFaction Faction { get => m_faction; set => m_faction = value; }

        public override bool TryJump(CTTDirection direction)
        {
            var posFront = BoardManager.Translate(m_position.Value, direction, 1);
            var expectedPosition = BoardManager.Translate(m_position.Value, direction, 2);

            // If posFront not occupied, fail.
            if (m_board.IsPositionEmpty(posFront)) return false;
            // If expectedPosition occupied, fail.
            // Else, jump.
            bool canIJump = m_board.TryPlace(this, expectedPosition); // If blocked, return false.
            // If the posFront is own faction, leave it be.
            // Else, capture.
            if (canIJump)
            {
                var piece = m_board[posFront];
                if(piece is Pawn)
                {
                    var pawn = (Pawn)piece;
                    if (pawn.Faction != m_faction)
                        // Capture
                        pawn.Remove();
                    // Else it's my own faction. Leave it be.
                }
                // Else don't do anything.
            }
            return canIJump;
        }
    }

    //public class Knight : Pawn
    //{
    //    public Knight(BoardManager board) : base(board)
    //    {
    //    }

    //    public Knight(BoardManager board, CTTFaction faction) : base(board, faction)
    //    {
    //    }

    //    public Knight(BoardManager board, Vector2 position, CTTFaction faction) : base(board, position, faction)
    //    {
    //    }

    //    public bool Try
    //}
}
